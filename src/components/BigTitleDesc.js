import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { Button, Dialog, Hidden, Typography } from "@material-ui/core";
import Zoom from "@material-ui/core/Zoom";

import "../index.css";
import SectionColorBackground from "./SectionColorBackground";

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    padding: "12rem 1.5rem",
    display: "flex",
    boxSizing: "border-box",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      padding: "5rem 1.5rem",
    },
  },
  imgContainer: {
    //maxWidth: "150px",
    zIndex: 100,
    width: "40%",
    [theme.breakpoints.down("sm")]: {
      width: "70%",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "100%",
    },
  },
  image: {
    width: "100%",
  },
  text: {
    zIndex: 10,
    fontSize: "5rem",
    fontWeight: "bold",
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      fontSize: "4.5rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "3rem",
    },
  },
  subtitle: {
    letterSpacing: "1.5px",
    zIndex: 10,
    fontSize: "1.3rem",
    textTransform: "uppercase",
    fontWeight: 300,
    marginBottom: "0.8rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
    },
  },
  desc: {
    width: "80%",
    zIndex: 10,
    fontSize: "1.5rem",
    marginTop: "1.4rem",
    textAlign: "center",
    fontWeight: 400,
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.7rem",
      marginTop: "2rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.2rem",
      width: "90%",
    },
  },
  button: {
    height: "50px",
    borderRadius: "25px",
    marginTop: "2rem",
    padding: "0 1.5rem",
  },
});

class BigTitleDesc extends Component {
  handlePage = (link) => () => {
    if (link.includes("http")) {
      window.open(link);
    } else {
      this.props.history.push(link);
    }
  };

  render() {
    const {
      classes,
      image,
      text,
      subtitle,
      desc,
      color,
      button,
      link,
      horizontal,
      vertical,
    } = this.props;

    return (
      <div className={classes.root}>
        <SectionColorBackground
          color={color}
          horizontal={horizontal}
          vertical={vertical}
        />
        {image && (
          <div className={classes.imgContainer}>
            <img src={image} alt={image} className={classes.image} />
          </div>
        )}
        {subtitle && (
          <Typography className={classes.subtitle}>{subtitle}</Typography>
        )}

        <Typography className={classes.text}>{text}</Typography>
        {desc && <Typography className={classes.desc}>{desc}</Typography>}
        {button && (
          <Button
            variant="contained"
            color="primary"
            onClick={this.handlePage(button.link)}
            className={classes.button}
          >
            {button.text}
          </Button>
        )}
      </div>
    );
  }
}
export default withRouter(withStyles(styles)(BigTitleDesc));
