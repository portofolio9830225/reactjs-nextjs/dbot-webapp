import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import withWidth from "@material-ui/core/withWidth";

import { withRouter } from "react-router-dom";
import { Typography, ButtonBase, Button, Hidden } from "@material-ui/core";
//import image from "../image/undraw_mobile_inbox_3h46.png";
import SectionColorBackground from "./SectionColorBackground";

const styles = (theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    width: "100%",
    boxSizing: "border-box",
    padding: "0 1.5rem",
    padding: "5rem 0",
    [theme.breakpoints.down("xs")]: {
      padding: "3rem 0",
    },
  },
  content: {
    width: "100%",
    maxWidth: "1400px",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
    },
  },
  contentInv: {
    width: "100%",
    maxWidth: "1400px",
    display: "flex",
    flexDirection: "row-reverse",
    justifyContent: "space-around",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column-reverse",
    },
  },
  textContainer: {
    zIndex: 10,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    width: "35%",
    // [theme.breakpoints.down("sm")]: {
    //   width: "45%",
    // },
    [theme.breakpoints.down("sm")]: {
      width: "85%",
      alignSelf: "center",
      marginTop: "3rem",
    },
  },

  header: {
    width: "100%",
    fontSize: "4.5rem",
    fontWeight: "bold",
    marginBottom: "2rem",
    [theme.breakpoints.down("md")]: {
      fontSize: "4rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "3rem",
      marginBottom: "1.2rem",
    },
  },
  pointContainer: {
    marginBottom: "1rem",
    padding: "0.7rem 0",
  },

  title: {
    fontWeight: "bold",
    fontSize: "1.6rem",
    marginBottom: "0.3rem",
    [theme.breakpoints.only("md")]: {
      fontSize: "1.5rem",
    },
    [theme.breakpoints.only("sm")]: {
      fontSize: "1.9rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.5rem",
    },
  },
  desc: {
    fontSize: "1.3rem",
    [theme.breakpoints.down("md")]: {
      fontSize: "1.1rem",
    },
    [theme.breakpoints.down("sm")]: {
      fontSize: "1.3rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
  button: {
    fontSize: "1.5rem",
    cursor: "pointer",
    textDecoration: "underline",
    color: "#23B9CB",
    fontWeight: 500,
    // textTransform: "capitalize",
    padding: "1.2rem 2rem",
    marginTop: "1rem",
    [theme.breakpoints.down("xs")]: {
      padding: "1rem 1.6rem",
    },
  },

  imgContainer: {
    zIndex: 10,
    width: "45%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      width: "70%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
  },
  image: {
    width: "100%",
    height: "100%",
  },
  subtitle: {
    letterSpacing: "1px",
    fontSize: "1.1rem",
    textTransform: "uppercase",
    fontWeight: 300,
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
});

class FeatureSection extends Component {
  handlePage = (page) => () => {
    this.props.history.push(page);
    // window.location.href = page;
  };

  render() {
    const {
      width,
      classes,
      inverted,
      title,
      desc,
      link,
      point,
      image,
      color,
      subtitle,
      vertical,
      horizontal,
      button_text,
    } = this.props;

    const Point = (props) => (
      <div className={classes.pointContainer}>
        {/* <Hidden xsDown> */}
        <Typography
          className={classes.title}
          style={{ textAlign: inverted ? "left" : "right" }}
        >
          {props.title}
        </Typography>

        <Typography
          className={classes.desc}
          style={{ textAlign: inverted ? "left" : "right" }}
        >
          {props.desc}
        </Typography>
      </div>
    );

    return (
      <div className={classes.root}>
        <SectionColorBackground
          color={color}
          horizontal={horizontal}
          vertical={vertical}
        />
        <div className={inverted ? classes.contentInv : classes.content}>
          <div className={classes.textContainer}>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems:
                  // width === "xs"
                  //   ? "flex-start"
                  //   :
                  inverted ? "flex-start" : "flex-end",
                alignSelf: "center",
              }}
            >
              {/* <Hidden xsDown> */}
              {subtitle && (
                <Typography className={classes.subtitle}>{subtitle}</Typography>
              )}
              <Typography
                className={classes.header}
                style={{ textAlign: inverted ? "left" : "right" }}
              >
                {title}
              </Typography>
              <Typography
                className={classes.desc}
                style={{ textAlign: inverted ? "left" : "right" }}
              >
                {desc}
              </Typography>
              {/* </Hidden>
              <Hidden smUp>
                <Typography
                  className={classes.header}
                  style={{ textAlign: "left" }}
                >
                  {title}
                </Typography>
              </Hidden> */}
              {point &&
                point.map((e, i) => (
                  <Point key={i} title={e.point_title} desc={e.point_desc} />
                ))}
              <Typography
                className={classes.button}
                style={inverted ? { paddingLeft: 0 } : { paddingRight: 0 }}
                color="primary"
                onClick={this.handlePage(link)}
              >
                {button_text}
              </Typography>
              {/* <Typography className={classes.button}>Learn more</Typography> */}
            </div>
          </div>
          <div className={classes.imgContainer}>
            <img className={classes.image} src={image} />
          </div>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(withRouter(withWidth()(FeatureSection)));
