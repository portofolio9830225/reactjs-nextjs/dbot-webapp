import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { Button, Dialog, Typography } from "@material-ui/core";
import Zoom from "@material-ui/core/Zoom";

import isEmpty from "../utils/isEmpty";

const styles = (theme) => ({
  dialog: {
    width: "100%",
    margin: 0,
    maxWidth: "800px",
    padding: "3rem 2rem",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      maxWidth: "500px",
      flexDirection: "column",
      padding: "1.5rem",
    },
    [theme.breakpoints.down("xs")]: {
      padding: "1.3rem",
      margin: "1rem",
    },
  },
  iconContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    width: "70%",
    borderRadius: "50%",
    [theme.breakpoints.down("sm")]: {
      width: "40%",
      marginBottom: "1rem",
    },
  },
  content: {
    width: "80%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    [theme.breakpoints.down("sm")]: {
      alignItems: "center",
      width: "90%",
    },
  },
  title: {
    fontFamily: "Poppins",
    fontWeight: 500,
    fontSize: "2rem",
    margin: 0,
    marginBottom: "1rem",
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.4rem",
    },
  },
  desc: {
    fontFamily: "Poppins",
    fontSize: "1.3rem",
    color: "grey",
    margin: 0,
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
  button: {
    color: "white",
    backgroundColor: "black",
    marginTop: "2rem",
    padding: "1.2rem 2rem",
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      marginTop: "1rem",
      padding: "1rem 1.5rem",
    },
  },
});

class PageModal extends Component {
  state = {
    welcome: false,
  };

  componentDidMount() {
    let isVisited = false;
    isVisited = sessionStorage.getItem(this.props.checker);
    if (!isVisited) {
      setTimeout(() => {
        this.setState(
          {
            welcome: true,
          },
          () => {
            sessionStorage.setItem(this.props.checker, true);
          }
        );
      }, 2000);
    }
  }

  handleWelcome = (link) => () => {
    this.setState({
      welcome: false,
    });
    if (!isEmpty(link)) {
      this.props.history.push(link);
      // window.location.href = link;
    }
  };

  render() {
    const {
      classes,
      checker,
      image,
      title,
      desc,
      buttonText,
      link,
    } = this.props;
    const { welcome } = this.state;

    return (
      <Dialog
        fullWidth
        TransitionComponent={Zoom}
        open={welcome}
        onBackdropClick={this.handleWelcome(null)}
        classes={{ paper: classes.dialog }}
      >
        <div className={classes.iconContainer}>
          <img src={image} className={classes.icon} />
        </div>

        <div className={classes.content}>
          <Typography className={classes.title}>{title}</Typography>
          <Typography className={classes.desc}>{desc}</Typography>
          <Button
            variant="contained"
            className={classes.button}
            onClick={this.handleWelcome(link)}
          >
            {buttonText}
          </Button>
        </div>
      </Dialog>
    );
  }
}
export default withRouter(withStyles(styles)(PageModal));
