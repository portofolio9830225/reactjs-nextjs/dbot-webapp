import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import withWidth from "@material-ui/core/withWidth";

const styles = (theme) => ({
  root: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  background: {
    position: "absolute",
    zIndex: 0,
  },
});

class SectionColorBackground extends Component {
  render() {
    const {
      width,
      classes,
      isHeader,
      color,
      horizontal,
      vertical,
    } = this.props;

    return (
      <div className={classes.root}>
        <div
          className={classes.background}
          style={{
            backgroundColor: color,
            width:
              width === "xs" || width === "sm" || horizontal === "full"
                ? "100%"
                : "75%",
            height:
              vertical === "full"
                ? isHeader
                  ? width === "sm" || width === "xs"
                    ? "80%"
                    : "100%"
                  : "100%"
                : "65%",
            top: vertical === "top" ? 0 : "auto",
            bottom: vertical === "bottom" ? 0 : "auto",
            right: horizontal === "right" ? 0 : "auto",
            left: horizontal === "left" ? 0 : "auto",
          }}
        />
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(
  withWidth()(SectionColorBackground)
);
