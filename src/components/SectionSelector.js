import React from "react";

import NavBar from "./mini/NavBar";
import Footer from "./mini/Footer";
import FeatureSection from "./FeatureSection";
import SponsorSection from "./SponsorSection";
import BigTitleDesc from "./BigTitleDesc";

import HomeHeader from "./HomeHeader";

const SectionSelector = (props) => {
  switch (props.data.type) {
    case 1:
      return (
        <HomeHeader
          image={props.data.image}
          title={props.data.title}
          desc={props.data.desc}
          subtitle={props.data.subtitle}
          buttons={props.data.buttons}
          color={props.data.background_color}
          vertical={props.data.vertical}
          horizontal={props.data.horizontal}
        />
      );
    // case 2:
    //   return (
    //     <CategorySection
    //       link={props.data.api_link}
    //       elementId="HomeCategory"
    //       boxWidth={250}
    //     />
    //   );
    case 3:
      return (
        <FeatureSection
          inverted={props.data.isInverted}
          title={props.data.title}
          desc={props.data.desc}
          button_text={props.data.button_text}
          link={props.data.action_link}
          image={props.data.image}
          point={props.data.point}
          subtitle={props.data.subtitle}
          color={props.data.background_color}
          vertical={props.data.vertical}
          horizontal={props.data.horizontal}
        />
      );
    case 4:
      return (
        <SponsorSection
          title={props.data.title}
          list={props.data.list}
          itemRowL={props.data.perRowL}
          itemRowS={props.data.perRowS}
          color={props.data.background_color}
        />
      );
    // case 5:
    //   return <BlogSection list={props.data.list} />;
    // case 6:
    //   return (
    //     <GoToDownloadSection
    //       inverted={props.data.isInverted}
    //       image={props.data.image}
    //       title={props.data.title}
    //       desc={props.data.desc}
    //       link={props.data.action_link}
    //       subtitle={props.data.subtitle}
    //       color={props.data.background_color}
    //       vertical={props.data.vertical}
    //       horizontal={props.data.horizontal}
    //       button_text={props.data.button_text}
    //     />
    //   );
    // case 7:
    //   return (
    //     <PageModal
    //       checker={props.data.checker}
    //       image={props.data.image}
    //       title={props.data.title}
    //       desc={props.data.desc}
    //       buttonText={props.data.button_text}
    //       link={props.data.action_link}
    //     />
    //   );
    case 8:
      return (
        <BigTitleDesc
          image={props.data.image}
          text={props.data.text}
          subtitle={props.data.subtitle}
          desc={props.data.desc}
          button={props.data.button}
          color={props.data.background_color}
          vertical={props.data.vertical}
          horizontal={props.data.horizontal}
        />
      );
    case 9:
      return (
        <NavBar
          homeLogo={props.data.logo}
          navs={props.data.navs}
          languages={props.data.languages}
        />
      );
    // case 10:
    //   return (
    //     <ExploreCategorySection
    //       title={props.data.title}
    //       link={props.data.api_link}
    //       color={props.data.color}
    //     />
    //   );
    // case 11:
    //   return (
    //     <DownloadSection
    //       title={props.data.title}
    //       download={props.data.download}
    //       color={props.data.background_color}
    //     />
    //   );

    // case 12:
    //   return <ListingPage images={props.data.images} info={props.data.info} />;
    // case 13:
    //   return (
    //     <AppBanner
    //       text={props.data.text}
    //       image={props.data.image}
    //       link={props.data.link}
    //     />
    //   );

    case 14:
      return <Footer list={props.data.contentList} />;

    default:
      return null;
  }
};

export default SectionSelector;
