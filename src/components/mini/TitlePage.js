import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Hidden from "@material-ui/core/Hidden";
import isEmpty from "../../utils/isEmpty";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    // alignItems: "center",
    justifyContent: "center",
    alignItems: "flex-start",
    alignSelf: "flex-start",
    // [theme.breakpoints.down("xs")]: {
    //   alignItems: "flex-start",
    //   alignSelf: "flex-start",
    // },
  },
  title: {
    fontFamily: "Poppins, sans-serif",
    // textAlign: "center",
    textAlign: "left",
    fontWeight: "bold",
    letterSpacing: "1px",
    // [theme.breakpoints.down("xs")]: {
    //   textAlign: "left",
    // },
  },
  divider: {
    borderBottom: "3px solid #29a9c6",
    width: "70%",
    // margin: "0.2rem auto 3rem"
  },
});

class Title extends Component {
  render() {
    const {
      classes,
      title,
      fontSizeL,
      fontSizeS,
      marginBottom,
      marginLeft,
      marginTop,
      marginRight,
    } = this.props;

    return (
      <div
        className={classes.root}
        style={{
          marginTop: marginTop ? marginTop : 0,
          marginRight: marginRight ? marginRight : 0,
          marginBottom: marginBottom ? marginBottom : "3rem",
          marginLeft: marginLeft ? marginLeft : 0,
        }}
      >
        <Hidden xsDown>
          <Typography
            className={classes.title}
            style={{
              fontSize: fontSizeL,
              color: !isEmpty(title) ? "black" : "transparent",
            }}
          >
            {!isEmpty(title) ? title : "."}
          </Typography>
        </Hidden>
        <Hidden smUp>
          <Typography
            className={classes.title}
            style={{
              fontSize: fontSizeS,
              color: !isEmpty(title) ? "black" : "transparent",
            }}
          >
            {!isEmpty(title) ? title : "."}
          </Typography>
        </Hidden>
        {/* <div className={classes.divider} /> */}
      </div>
    );
  }
}

export default withStyles(styles)(Title);
