import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { Fade, InputAdornment, TextField, withWidth } from "@material-ui/core";
import { black, grey, greydark, primary } from "../../utils/ColorPicker";
import {
  AddRounded,
  ChevronRightRounded,
  RemoveRounded,
} from "@material-ui/icons";
import isEmpty from "../../utils/isEmpty";
import content from "../../json/mini/footer.json";
import CollapsibleBox from "./CollapsibleBox";
import { extractFile } from "../../store/actions/jsonAction";

const styles = (theme) => ({
  root: {
    zIndex: 50,
    boxSizing: "border-box",
    width: "100%",
    padding: "4rem 1.5rem 1rem",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    //maxWidth: "1400px",
    [theme.breakpoints.down("xs")]: {
      padding: "2rem 0.5rem 0.5rem",
      alignItems: "center",
    },
  },
  content: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    maxWidth: "1400px",
    marginRight: "1rem",
    [theme.breakpoints.down("sm")]: {
      padding: "2rem 0",
      marginRight: "0",
      flexDirection: "column",
      alignItems: "center",
    },
  },
  left: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: "50%",
    [theme.breakpoints.down("sm")]: {
      width: "90%",
      flexDirection: "column",
    },
  },
  right: {
    display: "flex",
    flexDirection: "column",
    width: "50%",
    paddingRight: "6rem",
    boxSizing: "border-box",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      width: "90%",
      marginBottom: "4rem",
      paddingRight: 0,
    },
  },
  textContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      width: "90%",
      marginTop: "0.7rem",
    },
  },
  linkContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },

  linkText: {
    letterSpacing: "0.7px",
    fontSize: "1.1rem",
    cursor: "pointer",
    padding: "0.5rem 0 0.5rem 1rem",
    margin: "0.2rem",
    textDecoration: "underline",
    [theme.breakpoints.down("sm")]: {
      margin: "0.1rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
  text: {
    letterSpacing: "0.7px",
    fontSize: "1.1rem",
    margin: "0.2rem",
    color: black,
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      margin: "0.1rem",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
  linkIcon: {
    marginBottom: "0.5rem",
    paddingLeft: "1rem",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    cursor: "pointer",
    color: "black",
    [theme.breakpoints.down("xs")]: {
      padding: "0 0.5rem",
      marginBottom: 0,
    },
  },
  sectionTitle: {
    //color: "#888",
    letterSpacing: "1px",
    fontSize: "1.3rem",
    marginBottom: "0.7rem",
  },
  subsection: {
    width: "30%",
    display: "flex",
    flexDirection: "column",
    minWidth: "200px",
    marginBottom: "3rem",
  },
  socialSection: {
    display: "flex",
    flexDirection: "column",
    margin: "0 0 5rem 0",
    [theme.breakpoints.down("sm")]: {
      alignItems: "left",
      margin: "1.5rem 0 0 0",
    },
  },
  sectionTitle: {
    textTransform: "uppercase",
    fontWeight: 600,
    fontSize: "1.5rem",
    marginBottom: "1rem",
  },
  socialContent: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",

    [theme.breakpoints.down("xs")]: {
      width: "90%",
      justifyContent: "space-between",
    },
  },
  emailSection: {
    display: "flex",
    flexDirection: "column",
    margin: "0 0 4rem 0",
    [theme.breakpoints.down("sm")]: {
      alignItems: "left",
      marginTop: "2rem",
    },
  },
  textField: {
    width: "100%",
  },
  linkListContainer: {
    display: "flex",
    flexDirection: "column",
    width: "28%",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
    },
  },
  linkListContainerTitle: {
    fontSize: "1.3rem",
    textTransform: "uppercase",
    fontWeight: 600,
    marginBottom: "0.5rem",
  },
  linkListText: {
    fontSize: "1.1rem",
    fontWeight: 400,
    color: greydark,
    transition: "all 0.3s",
    cursor: "pointer",
    padding: "0.3rem",
    margin: "0.2rem 0",
    "&:hover": {
      color: black,
    },
  },
});

class Footer extends Component {
  state = {
    email: "",
    footerActive: null,
  };

  handlePage = (name) => () => {
    this.props.history.push(name);
    // window.location.pathname = name;
  };

  handleFilterFurniture = (name) => () => {
    this.props.history.push("/products", { filter: name });
  };

  handleSocial = (url) => () => {
    window.open(url);
  };

  handleText = (name) => (e) => {
    this.setState({
      [name]: e.target.value,
    });
  };

  // handleSubmitEmail = () => {
  //   if (emailChecker(this.state.email)) {
  //     this.setState({
  //       email: "",
  //     });
  //     this.props.sendEmailNewsletter({ email: this.state.email });
  //   } else {
  //     this.props.customNotification("Email not valid", "error");
  //   }
  // };

  handleCollapseFooter = (id) => () => {
    this.setState({
      footerActive: this.state.footerActive === id ? null : id,
    });
  };

  render() {
    const { width, classes, bgColor, position, list, socialMedia } = this.props;
    const { email, footerActive } = this.state;

    const LinkText = (props) => (
      <Typography
        className={classes.linkText}
        onClick={this.handlePage(props.page)}
      >
        {props.children}
      </Typography>
    );

    const LinkIcon = (props) => (
      <div
        style={props.style}
        onClick={this.handleSocial(props.link)}
        className={classes.linkIcon}
      >
        <i className={props.iconName} />
      </div>
    );

    const LinkList = (props) => {
      return (
        <Typography className={classes.linkListText} onClick={props.link}>
          {props.text}
        </Typography>
      );
    };

    const LinkListContainer = (props) => {
      return (
        <div className={classes.linkListContainter}>
          <Typography className={classes.linkListContainerTitle}>
            {props.title}
          </Typography>
          {props.list
            ? props.list.map((e, i) => (
                <LinkList
                  key={i}
                  link={this.handlePage(e.link)}
                  text={e.text}
                />
              ))
            : null}
        </div>
      );
    };

    const getSocMedIcon = (name) => {
      let val = "icon ";

      switch (name.toLowerCase()) {
        case "facebook":
          {
            val += "ion-logo-facebook";
          }
          break;
        case "linkedin":
          {
            val += "ion-logo-linkedin";
          }
          break;
        case "twitter":
          {
            val += "ion-logo-twitter";
          }
          break;
        case "instagram":
          {
            val += "ion-logo-instagram";
          }
          break;
        case "youtube":
          {
            val += "ion-logo-youtube";
          }
          break;
        default: {
          val = val;
        }
      }

      return val;
    };

    const CollapsibleHeader = (props) => {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            width: "100%",
          }}
        >
          {props.value !== footerActive ? <AddRounded /> : <RemoveRounded />}

          <Typography
            style={{
              fontSize: "1.5rem",
              marginBottom: 0,
              marginLeft: "0.5rem",
            }}
            className={classes.linkListContainerTitle}
          >
            {props.title}
          </Typography>
        </div>
      );
    };

    return (
      <div
        className={classes.root}
        style={{
          backgroundColor: "#f7f7f7",
          position: position ? position : "relative",
          bottom: position ? 0 : null,
        }}
      >
        <div className={classes.content}>
          {width === "xs" || width === "sm" ? (
            <div className={classes.left}>
              {content.contentList.map((e, i) => {
                return (
                  <CollapsibleBox
                    backgroundColor="transparent"
                    header={<CollapsibleHeader title={e.title} value={i} />}
                    active={footerActive}
                    value={i}
                    onClick={this.handleCollapseFooter(i)}
                  >
                    {e.subList
                      ? e.subList.map((e1, i1) => (
                          <LinkList
                            key={i1}
                            link={this.handlePage(e1.link)}
                            text={e1.text}
                          />
                        ))
                      : null}
                  </CollapsibleBox>
                );
                // return null;
              })}
            </div>
          ) : (
            <div className={classes.left}>
              {content.contentList
                ? content.contentList.map((e, i) => (
                    <LinkListContainer
                      key={i}
                      val={i}
                      title={e.title}
                      list={e.subList}
                    />
                  ))
                : null}
            </div>
          )}

          <div className={classes.right}>
            <div className={classes.socialSection}>
              <Typography className={classes.sectionTitle}>
                StoreUp on social media
              </Typography>
              <div className={classes.socialContent}>
                {content.socialMedia.map((e, i) => (
                  <LinkIcon
                    key={i}
                    iconName={getSocMedIcon(e.type)}
                    link={e.link}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className={classes.textContainer}>
          <Typography className={classes.text}>
            storeup.io is a product under Inonity Sdn Bhd
          </Typography>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(withRouter(withWidth()(Footer)));
