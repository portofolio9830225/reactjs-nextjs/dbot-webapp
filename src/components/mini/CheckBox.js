import React, { Component } from "react";
import { connect } from "react-redux";
import { withStyles } from "@material-ui/core/styles";

import { Checkbox, FormControlLabel } from "@material-ui/core";
import isEmpty from "../../utils/isEmpty";

const styles = (theme) => ({});

class CheckBox extends Component {
  state = {
    checked: !isEmpty(
      this.props.arr.find(
        (e) => e.type === this.props.type && e.name === this.props.label
      )
    ),
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.arr !== prevProps.arr) {
      this.setState({
        checked: !isEmpty(
          this.props.arr.find(
            (e) => e.type === this.props.type && e.name === this.props.label
          )
        ),
      });
    }
  }

  handleChange = (e) => {
    this.setState(
      {
        checked: e.target.checked,
      },
      () => {
        // let { arr } = this.props;
        // if (this.state.checked) {
        //   arr.push({ type: this.props.type, name: this.props.label });
        // } else {
        //   arr = arr.filter(
        //     (e) => e.name !== this.props.label || e.type !== this.props.type
        //   );
        // }

        this.props.onChange(
          { type: this.props.type, name: this.props.label },
          this.state.checked
        );
      }
    );
  };

  render() {
    const { classes, className, label, onChange } = this.props;
    const { checked } = this.state;

    return (
      <FormControlLabel
        className={className}
        control={
          <Checkbox
            color="primary"
            checked={checked}
            onChange={this.handleChange}
          />
        }
        label={label}
      />
    );
  }
}

export default withStyles(styles)(CheckBox);
