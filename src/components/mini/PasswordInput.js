import React from "react";
import TextInput from "./TextInput";

import { IconButton, TextField, InputAdornment } from "@material-ui/core";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

class PasswordInput extends React.Component {
	state = {
		showPass: false,
	};

	handleClickShowPassword = () => {
		this.setState({
			showPass: !this.state.showPass,
		});
	};

	handleMouseDownPassword = event => {
		event.preventDefault();
	};

	render() {
		const { showPass } = this.state;
		const { theme } = this.props;

		return (
			<TextInput
				{...this.props}
				type={showPass ? "text" : "password"}
				endAdornment={
					<IconButton
						aria-label="toggle password visibility"
						onClick={this.handleClickShowPassword}
						onMouseDown={this.handleMouseDownPassword}
					>
						{!showPass ? <Visibility /> : <VisibilityOff />}
					</IconButton>
				}
			/>
		);
	}
}

export default PasswordInput;
