import React, { Component } from "react";

import { withStyles } from "@material-ui/core/styles";
import withWidth from "@material-ui/core/withWidth";

import { withRouter } from "react-router-dom";
import { Typography, Hidden, Button, Collapse, Fade } from "@material-ui/core";
import DotIcon from "@material-ui/icons/FiberManualRecord";
import PlayStore from "../../images/googleplay.png";
import AppStore from "../../images/appstore.png";

const styles = (theme) => ({
  productBox: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    padding: "0.5rem 0",
    marginTop: "0.5rem",
  },

  productBoxContent: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    //alignItems: "center",
    justifyContent: "space-between",
    padding: "1rem 0",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },
  productBoxTitle: {
    fontSize: "1.7rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.3rem",
    },
  },
  headerContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    cursor: "pointer",
  },
  arrowIcon: {
    fontSize: "2rem",
    marginLeft: "1rem",
    transition: "all 0.3s",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.5rem",
    },
  },
  typeBox: {
    boxShadow:
      "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
    zIndex: 10,
    borderRadius: 10,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "49%",
    //maxWidth: "500px",
    boxSizing: "border-box",
    padding: "2.5rem 1rem",
    [theme.breakpoints.down("sm")]: {
      margin: "1.5rem 0",
      width: "100%",
      maxWidth: "auto",
    },
  },
  typeBoxImgContainer: {
    width: "50%",
  },
  typeBoxImg: {
    width: "100%",
  },
  typeBoxTextContainer: {
    width: "85%",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
  },
  typeBoxTitle: {
    fontWeight: "bold",
    textAlign: "center",
    fontSize: "2.5rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.5rem",
    },
  },
  typeBoxDesc: {
    textAlign: "left",
    fontSize: "1.4rem",
    margin: "1.5rem 0",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
  typeBoxPointContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "1rem",
  },
  dotIcon: {
    fontSize: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.5rem",
    },
  },
  typeBoxPoint: {
    fontWeight: 300,
    color: "dimgrey",
    textAlign: "center",
    fontSize: "1.3rem",
    marginLeft: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
      marginLeft: "0.5rem",
    },
  },
  typeBoxButton: {
    margin: "2rem 0",
    padding: "1rem 1.5rem",
    //width: "100%",
  },
});

class FadeBox extends Component {
  state = {
    val: this.props.val,
  };

  handlePage = (page) => () => {
    window.location.href = page;
  };

  render() {
    const { width, classes, color, active, title, arr, onClick } = this.props;
    const { val } = this.state;

    const TypeBox = (props) => (
      <div
        className={classes.typeBox}
        style={{ backgroundColor: props.backgroundColor }}
      >
        <div className={classes.typeBoxImgContainer}>
          <img className={classes.typeBoxImg} src={props.image} />
        </div>
        <div className={classes.typeBoxTextContainer}>
          <Typography className={classes.typeBoxTitle}>
            {props.title}
          </Typography>
          {props.desc && (
            <Typography className={classes.typeBoxDesc}>
              {props.desc}
            </Typography>
          )}
          {props.point &&
            props.point.map((e, i) => (
              <div className={classes.typeBoxPointContainer}>
                <DotIcon className={classes.dotIcon} />
                <Typography key={i} className={classes.typeBoxPoint}>
                  {e.text}
                </Typography>
              </div>
            ))}
          <Button
            className={classes.typeBoxButton}
            variant="contained"
            color="primary"
            onClick={this.handlePage(props.link)}
          >
            Download
          </Button>
        </div>
      </div>
    );
    if (val === active) {
      return (
        <div className={classes.productBox} onClick={onClick}>
          <Fade in={val === active} unmountOnExit>
            <div className={classes.productBoxContent}>
              {arr &&
                arr.map((e, i) => (
                  <TypeBox
                    key={i}
                    image={e.image === "ios" ? AppStore : PlayStore}
                    title={e.title}
                    desc={e.desc}
                    point={e.point}
                    link={e.action_link}
                    backgroundColor={e.background_color}
                  />
                ))}
            </div>
          </Fade>
        </div>
      );
    } else {
      return null;
    }
  }
}
export default withStyles(styles)(withRouter(withWidth()(FadeBox)));
