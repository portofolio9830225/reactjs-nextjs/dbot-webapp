import React from "react";
import { connect } from "react-redux";

import { InputBase, TextField, Typography } from "@material-ui/core";
import isEmpty from "../../utils/isEmpty";

class TextInput extends React.Component {
	// constructor(props) {
	// 	super(props);
	// 	// create a ref to store the textInput DOM element
	// 	this.TextInput = React.createRef();
	// }
	state = {
		errorKey: this.props.errorKey || "",
		errorText: "",
		text: "",
	};

	componentDidMount() {
		if (!isEmpty(this.props.error.errInput)) {
			let err = this.props.error.errInput.find(e => e.param === this.state.errorKey);
			if (!isEmpty(err)) {
				console.log(err);
			}
			this.setState({
				errorText: !isEmpty(err) ? err.error : "",
			});
		}
	}

	componentDidUpdate(prevProps, prevState) {
		if (prevProps.error.errInput !== this.props.error.errInput && !isEmpty(this.props.error.errInput)) {
			let err = this.props.error.errInput.find(e => e.param === this.state.errorKey);
			if (!isEmpty(err)) {
				console.log(err);
			}
			this.setState({
				errorText: !isEmpty(err) ? err.error : "",
			});
		}
		if (prevProps.value !== this.props.value) {
			this.setState({
				errorText: "",
			});
		}
	}

	handleChange = event => {
		this.setState(
			{
				text: event.target.value,
			},
			() => {
				//	this.TextInput.current.focus();

				this.props.onChange(event);
			}
		);
	};

	render() {
		const { errorText } = this.state;
		const { children, type } = this.props;

		return (
			<div
				style={{
					display: "flex",
					flexDirection: "row",
					justifyContent: "space-between",
					alignItems: "center",
					boxSizing: "border-box",
					paddingRight: !isEmpty(this.props.endAdornment) ? 0 : "16px",
					height: "50px",
					backgroundColor: "white",
					borderRadius: "10px",
					cursor: "text",
				}}
				className={this.props.className}
				onClick={() => {
					this.TextInput.focus();
				}}
			>
				<Typography style={{ width: "120px", paddingLeft: "16px" }}>{this.props.label}</Typography>
				{this.props.startAdornment ? this.props.startAdornment : null}
				<input
					style={{
						flex: 1,
						paddingLeft: "1rem",
						border: "none",
						background: "transparent",
						borderBottom: "1px solid #fff",
						outline: "none",
					}}
					onChange={this.handleChange}
					ref={input => (this.TextInput = input)}
					value={this.state.text}
					type={type}
				/>
				{this.props.endAdornment ? this.props.endAdornment : null}
			</div>
			// <TextField {...this.props} error={!isEmpty(errorText)} helperText={errorText}>
			// 	{children ? children : null}
			// </TextField>
		);
	}
}

const mapStateToProps = state => ({
	error: state.error,
});

export default connect(mapStateToProps)(TextInput);
