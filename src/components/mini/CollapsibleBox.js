import React, { Component } from "react";
import isEmpty from "../../utils/isEmpty";

import { withStyles } from "@material-ui/core/styles";

import { Collapse, Typography } from "@material-ui/core";
import { primary } from "../../utils/ColorPicker";
import {
  AddRounded,
  ExpandMoreRounded,
  RemoveRounded,
} from "@material-ui/icons";

const styles = (theme) => ({
  root: {
    //padding: "0.5rem 0",
    margin: "0.3rem 0",
    cursor: "pointer",
  },
  header: {
    display: "flex",
    flexDirection: "row",
    // justifyContent: "space-between",
    alignItems: "center",
    padding: "1rem 0.5rem",
    boxSizing: "border-box",
    cursor: "pointer",
    transition: "all 0.3s",

    "&:hover": {
      backgroundColor: "rgba(0,0,0,0.15)",
    },
  },
  headerTitle: {
    fontSize: "1.2rem",
    fontWeight: 600,
    letterSpacing: "0.3px",
  },
  icon: {
    fontSize: "1.7rem",
    marginRight: "0.7rem",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    padding: "1rem",
    paddingBottom: 0,
    boxSizing: "border-box",
  },
});

class CollapsibleBox extends Component {
  render() {
    const {
      classes,
      id,
      className,
      value,
      active,
      title,
      children,
      onClick,
      header,
      backgroundColor,
    } = this.props;

    let expanded = active === value;

    return (
      <div
        id={id}
        onClick={onClick}
        className={className ? className : classes.root}
        style={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
          boxSizing: "border-box",
          backgroundColor: backgroundColor
            ? backgroundColor
            : "rgba(0,0,0,0.05)",
        }}
      >
        {header ? (
          header
        ) : (
          <div className={classes.header}>
            {!expanded ? (
              <AddRounded className={classes.icon} />
            ) : (
              <RemoveRounded className={classes.icon} />
            )}
            <Typography className={classes.headerTitle}>{title}</Typography>
            {/* <ExpandMoreRounded
            style={{
              transition: "all 0.3s",
              transform: expanded ? "rotate(180deg)" : "rotate(0deg)",
            }}
          /> */}
          </div>
        )}
        <Collapse unmountOnExit mountOnEnter in={expanded}>
          <div className={classes.content}>{children}</div>
        </Collapse>
      </div>
    );
  }
}

export default withStyles(styles)(CollapsibleBox);
