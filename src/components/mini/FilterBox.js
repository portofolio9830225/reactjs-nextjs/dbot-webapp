import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { withStyles } from "@material-ui/core/styles";

import isEmpty from "../../utils/isEmpty";
import { ExpandMoreRounded, HighlightOffRounded } from "@material-ui/icons";
import { Collapse, Paper, Typography } from "@material-ui/core";
import CheckBox from "./CheckBox";

const styles = (theme) => ({
  filterBox: {
    display: "flex",
    flexDirection: "column",
    margin: "1rem 0",
    transition: "all 0.3s",
    borderColor: "transparent",
  },
  filterBoxTitle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    cursor: "pointer",
    paddingBottom: "0.5rem",
  },
  filterBoxTitleText: {
    textTransform: "uppercase",
    fontWeight: 600,
    fontSize: "1.1rem",
  },
  filterBoxList: {
    display: "flex",
    flexDirection: "column",
    paddingLeft: "0.7rem",
    margin: "0.5rem 0",
  },
  filterBoxListText: {
    // padding: "0.5rem 0",
    fontSize: "1rem",
    fontWeight: 500,
  },
  filterBoxListAlternate: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    margin: "0.5rem 0",
  },
  dropdownIcon: {
    transition: "all 0.3s",
  },
});

class FilterBox extends Component {
  state = {
    open: this.props.initialCollapse ? !this.props.initialCollapse : true,
    active: this.props.active ? this.props.active : "a",
  };

  handleCollapse = () => {
    if (this.state.active) {
      this.props.onCollapse(this.props.id);
    } else {
      this.setState({
        open: !this.state.open,
      });
    }
  };

  handleFilter = (data, checked) => {
    let array = [];
    if (checked) {
      array = [...this.props.arr, data];
    } else {
      array = this.props.arr.filter(
        (e) => e.name !== data.name || e.type !== data.type
      );
    }
    this.props.handleFilter(array);
  };

  handleRemoveFromFilter = (data) => () => {
    this.handleFilter(data, false);
  };

  render() {
    const {
      classes,
      active,
      id,
      last,
      title,
      list,
      checkbox,
      onClear,
      arr,
      noCollapse,
    } = this.props;

    const { open } = this.state;

    return (
      <div
        className={classes.filterBox}
        style={!last ? { borderBottom: "0.5px solid gainsboro" } : {}}
      >
        <div
          className={classes.filterBoxTitle}
          style={noCollapse ? { cursor: "default" } : null}
          onClick={noCollapse ? null : this.handleCollapse}
        >
          <Typography className={classes.filterBoxTitleText}>
            {title}
          </Typography>
          {!noCollapse ? (
            <ExpandMoreRounded
              className={classes.dropdownIcon}
              style={open ? { transform: "rotate(180deg)" } : null}
            />
          ) : null}
        </div>
        <Collapse unmountOnExit mountOnEnter in={active ? active === id : open}>
          <div className={classes.filterBoxList}>
            {list ? (
              checkbox ? (
                list.map((e, i) => (
                  <CheckBox
                    key={i}
                    arr={arr}
                    type={id}
                    label={e}
                    className={classes.filterBoxListText}
                    onChange={this.handleFilter}
                  />
                ))
              ) : (
                list.map((e, i) => (
                  <div key={i} className={classes.filterBoxListAlternate}>
                    <HighlightOffRounded
                      onClick={this.handleRemoveFromFilter(e)}
                      style={{
                        cursor: "pointer",
                        color: "grey",
                        fontSize: "1.3rem",
                        paddingRight: "0.5rem",
                      }}
                    />

                    <Typography key={i} className={classes.filterBoxListText}>
                      {e.name}
                    </Typography>
                  </div>
                ))
              )
            ) : (
              <div />
            )}
            {!checkbox && !isEmpty(list) ? (
              <Typography
                onClick={onClear}
                style={{
                  cursor: "pointer",
                  color: "firebrick",
                  fontWeight: 500,
                  margin: "0.5rem 0",
                }}
              >
                Reset all
              </Typography>
            ) : (
              <div />
            )}
          </div>
        </Collapse>
      </div>
    );
  }
}

export default withStyles(styles)(FilterBox);
