import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { AiFillCheckCircle } from "react-icons/ai";
import { Button } from "@material-ui/core";
import { RiNumber2 } from "react-icons/ri";
import { RiNumber3 } from "react-icons/ri";
import Grid from '@material-ui/core/Grid';


const styles = theme => ({ 
    root: {
        display: "flex",
        flexDirection: "column",
        position: "relative",
        width: "100%",
        marginTop: "1rem",
    },
    
    //riNumber2 with border circle 
    button: {
        height: "40px",
		// width: "100%",
		marginBottom: "1.5rem",
    },
    icon: {
        borderRadius: "50%",
        border: "2px solid #0D67F1",
        backgroundColor: "rgba(13, 103, 241, 0.2)",
        color: "#0D67F1",
        padding: "1rem",
    },
    iconCheck: {
        fontSize: "41px",
        color: "#0D67F1",
    },
    timelineContent: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "left",
        width: "100%",
        height: "100%",
    },
    timelineButton: {
        //center the button
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
    },
    //create vertical line
    line: {
        borderLeft: "2px solid #0D67F1",
        height: "50px",
        marginLeft: "1.5rem",
    },
    h3Top: {
        marginTop:"0px",
        marginBottom: "5px",
        // padding: "0px",
    },
});



//create component Timeline
class Timeline extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={0}>
                    <Grid item xs={2}>
                        <AiFillCheckCircle className={classes.iconCheck} />
                        <div className={classes.line}/>
                    </Grid>
                    <Grid item xs={10}>
                        <h3 className={classes.h3Top}>Your online store is ready!</h3>
                        
                    </Grid>
                </Grid>
                <Grid container spacing={0}>
                    <Grid item xs={2}>
                        <RiNumber2 className={classes.icon} />
                        <div className={classes.line}/>
                    </Grid>
                    <Grid item xs={10}>
                        <h3 className={classes.h3Top}>Add your first product</h3>
                        <Button className={classes.button} variant="contained" color="primary">
                            Submit
                        </Button>
                    </Grid>
                </Grid>
                <Grid container spacing={0}>
                    <Grid item xs={2}>
                        <RiNumber3 className={classes.icon} />
                        <div className={classes.line}/>
                    </Grid>
                    <Grid item xs={10}>
                        <h3 className={classes.h3Top}>Payout details</h3>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Timeline);