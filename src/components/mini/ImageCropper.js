import React, { Component } from "react";
import withWidth from "@material-ui/core/withWidth";
import isEmpty from "../../utils/isEmpty";
import ReactCrop from "react-image-crop";
import * as loadImage from "blueimp-load-image";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

import { Dialog, withMobileDialog } from "@material-ui/core";
import "react-image-crop/dist/ReactCrop.css";

const styles = (theme) => ({
  root: {
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  modalContent: {
    display: "flex",
    justifyContent: "center",
    width: "90%",
    padding: "3rem",
    [theme.breakpoints.down("xs")]: {
      padding: "1rem",
    },
  },
  dialogPaper: {
    backgroundColor: "rgba(0,0,0,0)",
    boxShadow: "none",
  },

  modalButtonContainer: {
    position: "fixed",
    bottom: 0,
    display: "flex",
    justifyContent: "center",
    margin: "2rem 0",
  },
  modalButtonO: {
    color: "white",
    borderColor: "white",
    margin: "0 0.5rem",
  },
  modalButtonC: {
    color: "black",
    backgroundColor: "white",
    margin: "0 0.5rem",
  },
  paperScrollBody: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    boxSizing: "border-box",
    margin: "auto",
    padding: "2rem 0",
    minHeight: "100vh",
    minWidth: "100vw",
    overflow: "hidden",
  },
});

class ImageCropper extends Component {
  constructor(props) {
    super(props);
    this.imagePreviewCanvasRef = React.createRef();
    this.fileInputRef = React.createRef();
  }

  state = {
    cropModal: false,
    croppedImageFile: null,
    croppedImageURL: null,
    file: null,
    fileName: null,
    fileExt: null,
    crop: {
      aspect: 1,
      width: 50,
      x: 0,
      y: 0,
    },
  };

  componentDidMount() {
    this.setState({
      crop: {
        ...this.state.crop,
        aspect: this.props.aspectRatio,
      },
    });
  }

  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  //react-crop START
  handleModal = (state) => () => {
    this.setState({
      cropModal: state,
      file: null,
    });
  };

  imageUpload = () => {
    document.getElementById(this.props.elementID).click();
  };

  handleSaveCrop = () => {
    this.setState({
      cropModal: false,
    });
    const { file } = this.state;
    if (file) {
      const canvasRef = this.imagePreviewCanvasRef.current;

      const { fileExt, fileName } = this.state;

      const imageData64 = canvasRef.toDataURL("image/" + fileExt);

      // file to be uploaded
      const myNewCroppedFile = this.base64StringtoFile(imageData64, fileName);

      this.setState(
        {
          croppedImageFile: myNewCroppedFile,
          croppedImageURL: imageData64,
        },
        () => {
          this.props.onSubmit(this.state.croppedImageFile, fileName);
        }
      );

      if (!isEmpty(this.fileInputRef.current.value)) {
        this.fileInputRef.current.value = null;
      }

      this.setState({
        file: null,
      });
    }
  };

  base64StringtoFile = (base64String, filename) => {
    var arr = base64String.split(","),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]),
      n = bstr.length,
      u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  };

  extractImageFileExtensionFromBase64 = (base64Data) => {
    return base64Data.substring(
      "data:image/".length,
      base64Data.indexOf(";base64")
    );
  };

  image64toCanvasRef = (canvasRef, image64, pixelCrop) => {
    const canvas = canvasRef; // document.createElement('canvas');
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");
    const image = new Image();
    image.src = image64;
    image.crossOrigin = "Anonymous";
    image.onload = () => {
      ctx.drawImage(
        image,
        pixelCrop.x,
        pixelCrop.y,
        pixelCrop.width,
        pixelCrop.height,
        0,
        0,
        pixelCrop.width,
        pixelCrop.height
      );
    };
  };

  onSelectImage = (e) => {
    this.setState({
      fileName: e.target.files[0].name,
      cropModal: true,
    });
    if (e.target.files && e.target.files.length > 0) {
      if (e.target.files[0].size < 10000000) {
        const reader = new FileReader();
        reader.addEventListener(
          "load",
          () => {
            loadImage(
              reader.result,
              (canvas) => {
                this.setState({
                  file: canvas.toDataURL(),
                });
              },
              { orientation: true }
            );
            this.setState({
              fileExt: this.extractImageFileExtensionFromBase64(reader.result),
            });
          },
          false
        );
        reader.readAsDataURL(e.target.files[0]);
      } else {
        this.setState({
          cropModal: false,
        });
        this.props.customNotification("Image size must be below 10MB", "error");
      }
    }
  };

  onImageLoaded = (image) => {
    this.setState({});
  };

  onCropChange = (crop) => {
    this.setState({ crop: crop });
  };

  onCropComplete = (crop, pixelCrop) => {
    const canvasRef = this.imagePreviewCanvasRef.current;
    const { file } = this.state;
    this.image64toCanvasRef(canvasRef, file, pixelCrop);
  };
  //react crop END

  render() {
    const { width, classes, fullScreen, children } = this.props;
    const { cropModal, file, crop, croppedImageURL } = this.state;

    return (
      <div className={classes.root}>
        <canvas hidden ref={this.imagePreviewCanvasRef} />
        <input
          ref={this.fileInputRef}
          type="file"
          accept="image/*"
          id={this.props.elementID}
          hidden
          onChange={this.onSelectImage}
        />
        <Dialog
          scroll="body"
          open={cropModal}
          fullScreen={fullScreen}
          classes={{
            paper: classes.dialogPaper,
            paperScrollBody: classes.paperScrollBody,
          }}
        >
          <div className={classes.modalContent}>
            <ReactCrop
              minWidth={25}
              src={file}
              crop={crop}
              onImageLoaded={this.onImageLoaded}
              onComplete={this.onCropComplete}
              onChange={this.onCropChange}
              keepSelection
              imageStyle={{ maxWidth: "100%", maxHeight: "70vh" }}
            />
          </div>
          <div className={classes.modalButtonContainer}>
            <Button
              variant="outlined"
              className={classes.modalButtonO}
              onClick={this.handleModal(false)}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              className={classes.modalButtonC}
              onClick={this.handleSaveCrop}
            >
              Save
            </Button>
          </div>
        </Dialog>

        {!isEmpty(croppedImageURL) ? (
          <div
            className={this.props.className}
            style={this.props.noBorder ? { border: "none" } : {}}
            onClick={this.imageUpload}
          >
            <img
              src={croppedImageURL}
              style={{
                maxWidth: "100%",
                maxHeight: "100%",
                border: this.props.noBorder ? "1px dashed gainsboro" : "none",
              }}
            />
          </div>
        ) : (
          <div className={this.props.className} onClick={this.imageUpload}>
            {children}
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(
  withMobileDialog({ breakpoint: "lg" })(withWidth()(ImageCropper))
);
