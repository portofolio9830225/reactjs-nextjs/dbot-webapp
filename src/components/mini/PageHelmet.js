import React, { Component } from "react";
import { Helmet } from "react-helmet";

class PageHelmet extends Component {
  render() {
    const { metadata } = this.props;

    return (
      <Helmet>
        <title>{metadata.title}</title>
        {metadata.keywords ? (
          <meta name="keywords" content={metadata.keywords} />
        ) : null}
        {metadata.description ? (
          <meta name="description" content={metadata.description} />
        ) : null}

        {metadata.link
          ? metadata.link.map((e, i) => (
              <link key={i} rel={e.rel} hrefLang={e.hreflang} href={e.href} />
            ))
          : null}
      </Helmet>
    );
  }
}

export default PageHelmet;
