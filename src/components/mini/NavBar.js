import React, { Component } from "react";
import { connect } from "react-redux";

import { withStyles } from "@material-ui/core/styles";
import Zoom from "@material-ui/core/Zoom";
import Hidden from "@material-ui/core/Hidden";
import Drawer from "@material-ui/core/Drawer";

import MenuIcon from "@material-ui/icons/MenuRounded";
import CloseIcon from "@material-ui/icons/CloseRounded";
import { withRouter } from "react-router-dom";
import { Typography, Slide, Paper, Button } from "@material-ui/core";
import isEmpty from "../../utils/isEmpty";
import { Waypoint } from "react-waypoint";
import { logoutUser } from "../../store/actions/authAction";
import { mainBgColor } from "../../utils/ColorPicker";

const styles = (theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    position: "relative",
    width: "100%",
  },
  waypointContainer: {
    height: "150px",
    position: "absolute",
    zIndex: -1,
    width: "100%",
  },
  navBar: {
    width: "100%",
    position: "fixed",
    top: 0,
    zIndex: 1000,
    backgroundColor: mainBgColor,
  },
  content: {
    zIndex: 100,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: "75px",
    width: "100%",
    boxSizing: "border-box",
    padding: "0 2rem",
    backgroundColor: "transparent",
    [theme.breakpoints.down("xs")]: {
      padding: "0 1.5rem",
    },
  },
  logo: {
    height: "40%",
    cursor: "pointer",
    paddingLeft: "0.5rem",
  },
  navContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  navMenu: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    padding: "0.7rem 0",
  },
  navButton: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "2rem",
    padding: "0.7rem 1rem",
    borderRadius: 10,
    transition: "all 0.3s",
    letterSpacing: "0.5px",
    fontSize: "1rem",
    color: "black",
    "&:hover": {
      backgroundColor: "gainsboro",
      cursor: "pointer",
    },
  },
  navButtonInv: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "2rem",
    padding: "0.7rem 1rem",
    borderRadius: 10,
    transition: "all 0.3s",
    letterSpacing: "0.5px",
    fontSize: "1rem",
    color: "white",
    "&:hover": {
      backgroundColor: "dimgrey",
      cursor: "pointer",
    },
  },
  langIcon: {
    fontSize: "1.5rem",
  },
  menuDrawer: {
    height: "100vh",
    backgroundColor: "rgba(0,0,0,1)",
    display: "flex",
    flexDirection: "column",
    padding: "1rem",
    boxSizing: "border-box",
  },
  closeIcon: {
    alignSelf: "flex-end",

    padding: "1rem",
    paddingRight: "0.3rem",
    fontSize: "2.5rem",
    marginBottom: "1rem",
    cursor: "pointer",
  },
  navList: {
    display: "flex",
    flexDirection: "column",
  },
  navText: {
    color: "white",
    fontSize: "2.5rem",
    letterSpacing: "1px",
    fontWeight: "bold",
    padding: "0.7rem 0.5rem",
    cursor: "pointer",
  },
});

class NavBar extends Component {
  state = {
    show: false,
    menu: false,
    langModal: null,
  };

  handleNavBar = ({ previousPosition, currentPosition, event }) => {
    this.setState({
      show: currentPosition !== Waypoint.inside,
    });
  };

  handlePage = (link) => () => {
    this.setState({
      menu: false,
      langModal: null,
    });
    if (link.includes("http")) {
      window.open(link);
    } else {
      this.props.history.push(link);
    }
  };

  handleMenu = (state) => () => {
    this.setState({
      menu: state,
    });
  };

  handleLangModal = (state) => (e) => {
    this.setState({
      langModal: state ? e.currentTarget : null,
    });
  };

  handleLogout = () => {
    this.props.logoutUser();
  };

  render() {
    const { menu, show } = this.state;
    const { classes, inverted, homeLogo, navs, languages, logout } = this.props;

    const NavButton = (props) => {
      return window.location.pathname !== props.link ? (
        <Button
          disableRipple
          size="large"
          color="primary"
          style={{
            margin: "0.5rem 0",
            padding: "0 1rem",
            fontFamily: "Poppins",
            textTransform: "none",
            fontSize: "1.1rem",
            fontWeight: 400,
            letterSpacing: "0.5px",
          }}
          onClick={props.action}
        >
          {props.icon ? props.icon : null}
          {props.name}
        </Button>
      ) : null;
    };

    const Content = (props) => (
      <div className={classes.content}>
        <div
          className={classes.logo}
          onClick={this.handlePage(homeLogo.action_link)}
        >
          <img
            style={{ height: "100%" }}
            src={homeLogo.image}
            alt={homeLogo.image}
          />
        </div>
        <div className={classes.navContainer}>
          <Hidden xsDown>
            <div className={classes.navMenu}>
              {!isEmpty(navs)
                ? navs.map((e, i) => (
                    <NavButton
                      key={i}
                      name={e.title}
                      action={this.handlePage(e.action_link)}
                      icon={e.icon ? e.icon : null}
                    />
                  ))
                : null}
              {logout ? (
                <NavButton name="Logout" action={this.handleLogout} />
              ) : null}
            </div>
          </Hidden>
          {/* {languages && (
            <ButtonWithMenu list={languages}>
              <LanguangeIcon
                className={classes.langIcon}
                style={{ color: inverted ? "darkgrey" : "dimgrey" }}
              />
              <ChevronDownIcon
                className={classes.langIcon}
                style={{ color: inverted ? "darkgrey" : "dimgrey" }}
              />
            </ButtonWithMenu>
          )} */}
          {isEmpty(navs) && !logout ? null : (
            <Hidden smUp>
              <Zoom
                in={!menu}
                style={{ transitionDelay: !menu ? "150ms" : "0ms" }}
              >
                <MenuIcon
                  style={{
                    paddingLeft: "1rem",
                    cursor: "pointer",
                    color: inverted ? "white" : "black",
                  }}
                  onClick={this.handleMenu(true)}
                />
              </Zoom>
            </Hidden>
          )}
        </div>
      </div>
    );

    return (
      <div className={classes.root}>
        <Slide direction="down" in={show} mountOnEnter unmountOnExit>
          <Paper square className={classes.navBar}>
            <Content />
          </Paper>
        </Slide>
        <Waypoint onPositionChange={this.handleNavBar}>
          <div className={classes.waypointContainer} />
        </Waypoint>
        <Content />

        <Hidden smUp>
          <Drawer
            classes={{ paper: classes.menuDrawer }}
            anchor="top"
            open={menu}
            onClose={this.handleMenu(false)}
          >
            <Zoom in={menu} style={{ transitionDelay: menu ? "170ms" : "0ms" }}>
              <CloseIcon
                style={{ color: "white" }}
                className={classes.closeIcon}
                onClick={this.handleMenu(false)}
              />
            </Zoom>

            <div className={classes.navList}>
              {!isEmpty(navs)
                ? navs.map((e, i) => (
                    <Typography
                      key={i}
                      className={classes.navText}
                      onClick={this.handlePage(e.action_link)}
                    >
                      {e.title}
                    </Typography>
                  ))
                : null}
              {logout ? (
                <Typography
                  className={classes.navText}
                  onClick={this.handleLogout}
                >
                  Logout
                </Typography>
              ) : null}
            </div>
          </Drawer>
        </Hidden>
      </div>
    );
  }
}
export default connect(null, { logoutUser })(
  withStyles(styles)(withRouter(NavBar))
);
