import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import { Typography, Button } from "@material-ui/core";
import SectionColorBackground from "./SectionColorBackground";

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    boxSizing: "border-box",
    minHeight: "600px",
    height: "80vh",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    [theme.breakpoints.down("md")]: {
      height: "60vh",
    },
    [theme.breakpoints.down("sm")]: {
      height: "auto",
    },
  },
  content: {
    boxSizing: "border-box",
    height: "100%",
    width: "100%",
    maxWidth: "1400px",
    // padding: "0 1.5rem",
    //maxWidth: "1400px",
    //margin: "5rem 0",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    //height: "100%",
    //backgroundColor: "rgba(100,100,100,1)",
    //backgroundClip: "initial",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      paddingTop: "3rem",
    },
  },
  textContainer: {
    zIndex: 10,
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    width: "55%",
    [theme.breakpoints.down("md")]: {
      width: "50%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "90%",
      alignSelf: "center",
      marginTop: "1rem",
    },
  },
  title: {
    fontSize: "4.8rem",
    fontWeight: "bold",
    // margin: "1.8rem 0 2.7rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: "4.4rem",
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      marginTop: "1rem",
      fontSize: "2.8rem",
    },
  },
  subtitle: {
    letterSpacing: "1px",
    fontSize: "1.3rem",
    margin: "1rem 0 0",
    // textTransform: "uppercase",
    fontWeight: 300,
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
    },
  },
  desc: {
    letterSpacing: "1px",
    fontSize: "1.3rem",
    margin: "1rem 0 2.5rem",
    // textTransform: "uppercase",
    fontWeight: 300,
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
    },
  },
  imgContainer: {
    zIndex: 100,
    width: "35%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("md")]: {
      width: "40%",
    },
    [theme.breakpoints.down("sm")]: {
      width: "60%",
    },
    [theme.breakpoints.down("xs")]: {
      width: "90%",
    },
  },
  image: {
    width: "100%",
    height: "auto",
  },
  buttonContainer: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    [theme.breakpoints.down("sm")]: {
      justifyContent: "center",
      margin: "3rem 0",
    },
    [theme.breakpoints.down("xs")]: {
      margin: "2.5rem 0",
      justifyContent: "center",
      alignItem: "center",
    },
  },
  button: {
    height: "50px",
    borderRadius: "25px",
    padding: "0.8rem 1.3rem",
    margin: "0.5rem 0.5rem",
    minWidth: "30%",
    [theme.breakpoints.down("xs")]: {
      minWidth: "auto",
      width: "100%",
    },
  },
  imageButtonContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    padding: "1rem",
    boxSizing: "border-box",
    cursor: "pointer",
    [theme.breakpoints.down("xs")]: {
      padding: "0.5rem",
    },
  },
  imageButtonImgContainer: {
    width: "50px",
    [theme.breakpoints.down("xs")]: {
      width: "35px",
    },
  },
  imageButtonImg: {
    width: "100%",
  },
  imageButtonText: {
    marginLeft: "1rem",
    fontSize: "1.3rem",
    fontWeight: 500,
    borderBottom: "1px solid black",
    paddingBottom: "2px",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
});

class HomeHeader extends Component {
  handlePage = (page) => () => {
    this.props.history.push(page);
  };
  render() {
    const {
      classes,
      image,
      title,
      desc,
      buttons,
      color,
      vertical,
      horizontal,
      subtitle,
    } = this.props;

    const ImageButton = (props) => (
      <div
        className={classes.imageButtonContainer}
        onClick={props.onClick}
        style={{ paddingLeft: 0 }}
      >
        <div className={classes.imageButtonImgContainer}>
          <img src={props.image} className={classes.imageButtonImg} />
        </div>
        <Typography
          className={classes.imageButtonText}
          style={{ color: props.color, borderBottomColor: props.color }}
        >
          {props.text}
        </Typography>
      </div>
    );

    const HomeHeaderButton = (props) => {
      switch (props.type) {
        case 1:
          return (
            <Button
              className={classes.button}
              color="primary"
              variant={props.variant}
              onClick={props.onClick}
            >
              {props.children}
            </Button>
          );
        case 2:
          return (
            <ImageButton
              image={props.image}
              text={props.children}
              onClick={props.onClick}
            />
          );
        default:
          return (
            <Button
              className={classes.button}
              color="primary"
              variant={props.variant}
              onClick={props.onClick}
            >
              {props.children}
            </Button>
          );
      }
    };

    return (
      <div className={classes.root}>
        <SectionColorBackground
          //   isHeader
          color={color}
          horizontal={horizontal}
          vertical={vertical}
        />

        <div className={classes.content}>
          <div className={classes.imgContainer}>
            <img className={classes.image} src={image} />
          </div>
          <div className={classes.textContainer}>
            <div style={{ alignSelf: "center" }}>
              <Typography className={classes.subtitle}>{subtitle}</Typography>
              <Typography className={classes.title}>{title}</Typography>
              <Typography className={classes.desc}>{desc}</Typography>
              <div className={classes.buttonContainer}>
                {buttons &&
                  buttons.map((e, i) => (
                    <HomeHeaderButton
                      key={i}
                      variant={e.variant}
                      onClick={this.handlePage(e.action_link)}
                      type={e.type}
                      image={e.image}
                    >
                      {e.text}
                    </HomeHeaderButton>
                  ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(withStyles(styles, { withTheme: true })(HomeHeader));
