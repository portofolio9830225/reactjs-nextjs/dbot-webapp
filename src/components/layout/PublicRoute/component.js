import React from "react";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import { Typography, Slide, Dialog } from "@material-ui/core/";
import { connect } from "react-redux";
import { clearNotification } from "../../../store/actions/notiAction";
import navbar from "../../../json/mini/navbar.json";

import Footer from "../../mini/Footer";
import NavBar from "../../mini/NavBar";
import Loading from "../../mini/Loading";
import SnackBar from "../../mini/SnackBar";
import { mainBgColor } from "../../../utils/ColorPicker";

const styles = theme => ({
	root: {
		flexGrow: 1,
		zIndex: 1,

		position: "relative",
		display: "flex",
		flexDirection: "column",
		width: "100%",
		minHeight: "100vh",
		backgroundColor: mainBgColor,
	},
	content: {
		display: "flex",
		flex: 1,
		flexDirection: "column",
	},
	main: {
		display: "flex",
		flexDirection: "column",
		minHeight: "70vh",
		flex: 1,
	},
	DialogRoot: {
		backgroundColor: "rgba(0,0,0,0)",
		boxShadow: "none",
		display: "flex",
		borderRadius: 0,
	},
});

class PublicComponent extends React.Component {
	render() {
		const { classes, noNavbar } = this.props;
		const loading = this.props.loading.status;

		return (
			<div className={classes.root}>
				<SnackBar />

				<div className={classes.content}>
					<Dialog
						open={loading}
						elevation={0}
						classes={{
							paper: classes.DialogRoot,
						}}
					>
						<Loading open={loading} />
					</Dialog>

					{!noNavbar ? (
						<NavBar homeLogo={navbar.logo} navs={navbar.navs} languages={navbar.languages} />
					) : null}
					<main className={classes.main}>{this.props.children}</main>
					{/* <Footer /> */}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	loading: state.loading,
});

export default connect(mapStateToProps, { clearNotification })(
	withStyles(styles, { withTheme: true })(withRouter(PublicComponent))
);
