import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import isEmpty from "../utils/isEmpty";

import { withStyles } from "@material-ui/core/styles";
import { Typography, withWidth } from "@material-ui/core";
import SectionColorBackground from "./SectionColorBackground";

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    height: "auto",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: "2.5rem 0",
  },
  title: {
    textTransform: "uppercase",
    letterSpacing: "1.5px",
    fontWeight: 300,
    zIndex: 10,
    fontSize: "1.3rem",
    color: "dimgrey",
    margin: "1rem 0",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
  content: {
    zIndex: 10,
    width: "100%",
    maxWidth: "900px",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    flexFlow: "wrap",
  },
  imgContainer: {
    padding: "1rem 2rem",
    boxSizing: "border-box",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      padding: "1.5rem 1rem",
    },
  },
  img: {
    width: "100%",
  },
});

class SponsorSection extends Component {
  getImgWidth = (num) => {
    switch (num) {
      case 1:
        return "55%";
      case 2:
        return "35%";
      case 3:
        return "28%";
      case 4:
        return "25%";
      case 5:
        return "20%";
      default:
        return "28%";
    }
  };

  handlePage = (page) => () => {
    this.props.history.push(page);
    // window.location.href = page;
  };

  render() {
    const {
      width,
      classes,
      title,
      list,
      itemRowL,
      itemRowS,
      color,
      vertical,
      horizontal,
    } = this.props;

    const ListBox = (props) => (
      <div
        className={classes.imgContainer}
        style={{
          width:
            width === "xs"
              ? this.getImgWidth(itemRowS)
              : this.getImgWidth(itemRowL),
          cursor: props.link && !isEmpty(props.link) ? "pointer" : "default",
        }}
        onClick={
          props.link && !isEmpty(props.link)
            ? this.handlePage(props.link)
            : null
        }
      >
        <img className={classes.img} src={props.img} alt={props.img} />
      </div>
    );

    return (
      <div className={classes.root}>
        <SectionColorBackground
          color={color}
          horizontal="full"
          vertical="full"
        />
        <Typography className={classes.title}>{title}</Typography>
        <div className={classes.content}>
          {!isEmpty(list)
            ? list.map((e, i) => (
                <ListBox key={i} img={e.image} link={e.action_link} />
              ))
            : null}
        </div>
      </div>
    );
  }
}

export default withRouter(
  withStyles(styles, { withTheme: true })(withWidth()(SponsorSection))
);
