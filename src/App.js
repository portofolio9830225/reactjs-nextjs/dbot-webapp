import React, { Component } from "react";
import { Switch } from "react-router-dom";
import PublicRoute from "./components/layout/PublicRoute";

import PageNotFound from "./pages/PageNotFound";
import VerifyByEmail from "./pages/VerifyByEmail";
import RPassword from "./pages/RPassword";

import Home from "./pages/Home";

class App extends Component {
	render() {
		return (
			<div id="HOC">
				<Switch>
					{/* <PublicRoute
            exact
            path="/"
            render={() => {
              // if (process.env.NODE_ENV === "production") {
              //   window.location.href = "https://storeup.io";
              // } else {
              window.location.replace("https://storeup.io");
              // }

              return null;
            }}
          /> */}
					<PublicRoute exact path="/" component={Home} noNavbar />
					<PublicRoute exact path="/activate-account" component={VerifyByEmail} noNavbar />
					<PublicRoute exact path="/set-password" component={RPassword} noNavbar />
					<PublicRoute component={PageNotFound} noNavbar />
				</Switch>
			</div>
		);
	}
}

export default App;
