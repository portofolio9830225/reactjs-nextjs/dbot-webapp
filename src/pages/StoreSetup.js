import React, { Component } from "react";
import { connect } from "react-redux";
import { customNotification } from "../store/actions/notiAction";

import { withStyles } from "@material-ui/core/styles";
import { Button, Fade, TextField, Typography } from "@material-ui/core";
import PageHelmet from "../components/mini/PageHelmet";
import isEmpty from "../utils/isEmpty";
import { IoMdColorWand } from "react-icons/io";
import { setLoading } from "../store/actions/loadingAction";
import { mainBgColor, primary } from "../utils/ColorPicker";
import { editStoreDetails, loginUser, sendUserEmail } from "../store/actions/authAction";

import Timeline from "../components/mini/Timeline";

class Home extends Component {
	state = {
		step: 1,
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevState.step !== this.state.step) {
			this.props.setLoading(false);
		}
		if (isEmpty(prevProps.noti.redirect) && !isEmpty(this.props.noti.redirect)) {
			if (this.props.noti.redirect === "passcode") {
				this.setState(
					{
						step: 3,
					},
					() => {
						this.passcodeInput.focus();
					}
				);
			}
			if (this.props.noti.redirect === "StoreSaved") {
				this.setState({
					step: 5,
				});
			}
		}
		if (isEmpty(prevProps.auth.business) && !isEmpty(this.props.auth.business)) {
			this.setState({
				step: !isEmpty(this.props.auth.business.name) ? 5 : 4,
			});
		}
	}

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			this.props.history.push(link);
		}
	};

	handleChange =
		(name, limit = null) =>
		event => {
			let val = event.target.value;
			if (!isEmpty(limit) && val.length > limit) {
				val = val.slice(0, limit);
			}

			this.setState({
				[name]: val,
			});
		};

	titleText = () => {
		switch (this.state.step) {
			case 1:
				return "Sign In";
			case 2:
				return "Registration";
			case 3:
				return "Passcode";
			case 4:
				return "Business details";
			case 5:
				return "Download app";
		}
	};

	render() {
		const { classes } = this.props;
		const { step } = this.state;
		const StoreSetup = step === 1;

		const PageContentHeader = ({ title, icon, desc, ...props }) => {
			let margin = "2rem";
			return (
				<>
					{icon}
					<Typography
						style={{
							fontSize: "32px",
							fontWeight: 700,
							textAlign: "left",
							marginBottom: desc ? 0 : margin,
						}}
					>
						{title}
					</Typography>
					{desc ? (
						<Typography
							style={{ fontSize: "16px", fontWeight: 400, textAlign: "center", marginBottom: margin }}
						>
							{desc}
						</Typography>
					) : null}
				</>
			);
		};

		return (
			<div className={classes.root}>
				<PageHelmet metadata={{ title: `${this.titleText()} | StoreUp` }} />
				<div className={classes.container}>
						<Fade in={StoreSetup} unmountOnExit mountOnEnter>
							<div className={classes.innerContainer}>
								<PageContentHeader
									title="Complete your
                                    store setup..."
									icon={<IoMdColorWand className={classes.icon} />}
								/>
								<Typography className={classes.detail}>
                                Sellers who add more than 25 products are likely to get more orders. Add all your products to your store now.
                                </Typography>
                                <Timeline className={classes.icon}/>
							</div>
						</Fade>
				</div>
			</div>
		);
	}
}

const styles = theme => ({
	root: {
		alignSelf: "center",
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		maxWidth: "500px",
		height: "100vh",
		minHeight: "600px",
		//padding: "5rem 1rem 0",
		padding: "0 1rem 0",
		position: "relative",
		backgroundColor: mainBgColor,
	},
	container: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: "100%",
	},
	innerContainer: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "left",
		width: "100%",
	},
	icon: {
		fontSize: "10rem",
		color: primary,
		marginBottom: "1rem",
	},
	textButton: {
		color: primary,
		fontSize: "14px",
		cursor: "pointer",
	},
	button: {
		height: "40px",
		width: "100%",
		marginBottom: "1.5rem",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	noti: state.noti,
	error: state.error,
});

export default connect(mapStateToProps, {
	sendUserEmail,
	loginUser,
	editStoreDetails,
	customNotification,
	setLoading,
})(withStyles(styles)(Home));
