import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";

import image404 from "../images/page404.svg";
import { Typography } from "@material-ui/core";
import { Helmet } from "react-helmet";
import PageHelmet from "../components/mini/PageHelmet";

const styles = (theme) => ({
  root: {
    width: "100%",
    minHeight: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
  },
  content: {
    height: "50vh",
    minHeight: "500px",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  logo: {
    height: "100%",
    width: "80%",
    maxHeight: "300px",
  },
  message: {
    width: "80%",
    maxWidth: "600px",
    marginTop: "2rem",
    fontSize: "2rem",
    textAlign: "center",
    [theme.breakpoints.down("sm")]: {
      marginTop: "1rem",
    },
  },
});

class PageNotFound extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <PageHelmet metadata={{ title: "Page not found | StoreUp" }} />

        <div className={classes.content}>
          <img src={image404} className={classes.logo} />
          <Typography className={classes.message}>
            Oops! Looks like the page that you search appeared to be missing.
          </Typography>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(PageNotFound);
