import React, { Component } from "react";
import { connect } from "react-redux";
import { customNotification } from "../store/actions/notiAction";

import { withStyles } from "@material-ui/core/styles";
import { Button, Fade, TextField, Typography } from "@material-ui/core";
import PageHelmet from "../components/mini/PageHelmet";
import isEmpty from "../utils/isEmpty";
import { BiKey } from "react-icons/bi";
import { BsPersonFill, BsCheckCircleFill } from "react-icons/bs";
import { IoMdMail } from "react-icons/io";
import { IoBusiness } from "react-icons/io5";
import { setLoading } from "../store/actions/loadingAction";
import TextInput from "../components/mini/TextInput";
import { mainBgColor, primary } from "../utils/ColorPicker";
import { editStoreDetails, loginUser, sendUserEmail } from "../store/actions/authAction";

class Home extends Component {
	state = {
		step: 1,
		email: "",
		passcode: "",
		bName: "",
		bPhone: "",
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevState.step !== this.state.step) {
			this.props.setLoading(false);
		}
		if (isEmpty(prevProps.noti.redirect) && !isEmpty(this.props.noti.redirect)) {
			if (this.props.noti.redirect === "passcode") {
				this.setState(
					{
						step: 3,
					},
					() => {
						this.passcodeInput.focus();
					}
				);
			}
			if (this.props.noti.redirect === "StoreSaved") {
				this.setState({
					step: 5,
				});
			}
		}
		if (isEmpty(prevProps.auth.business) && !isEmpty(this.props.auth.business)) {
			this.setState({
				step: !isEmpty(this.props.auth.business.name) ? 5 : 4,
			});
		}
	}

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			this.props.history.push(link);
		}
	};

	handleChange =
		(name, limit = null) =>
		event => {
			let val = event.target.value;
			if (!isEmpty(limit) && val.length > limit) {
				val = val.slice(0, limit);
			}

			this.setState({
				[name]: val,
			});
		};

	titleText = () => {
		switch (this.state.step) {
			case 1:
				return "Sign In";
			case 2:
				return "Registration";
			case 3:
				return "Passcode";
			case 4:
				return "Business details";
			case 5:
				return "Download app";
		}
	};

	handleSendEmail = () => {
		let data = {
			email: this.state.email.toLowerCase(),
		};
		this.props.sendUserEmail(data);
	};

	handleSendPasscode = () => {
		let data = {
			email: this.state.email.toLowerCase(),
			passcode: this.state.passcode,
		};
		console.log(data);
		this.props.loginUser(data);
	};

	handleSendBusiness = () => {
		let { business } = this.props.auth;
		let data = {
			name: this.state.bName,
			phone: this.state.bPhone,
			subdomain: business.subdomain.toLowerCase(),
			logo: business.logo,
			registered_name: business.registered_name,
			ssm_no: business.ssm_no,
			email: business.email.toLowerCase(),
			description: business.description,
		};
		this.props.editStoreDetails(business.id, data, this.props.auth.token);
	};

	handleDownloadApp = () => {
		window.location.href = "https://go.storeup.io/download-app";
	};

	handleSubmit = () => {
		switch (this.state.step) {
			case 1:
			case 2:
				this.handleSendEmail();
				break;
			case 3:
				this.handleSendPasscode();
				break;
			case 4:
				this.handleSendBusiness();
				break;
			case 5:
				this.handleDownloadApp();
				break;
		}
	};

	render() {
		const { classes } = this.props;
		const { step, email, passcode, bName, bPhone } = this.state;
		const LoginPage = step === 1;
		const RegisterPage = step === 2;
		const PasscodePage = step === 3;
		const BusinessPage = step === 4;
		const SuccessPage = step === 5;

		const PageContentHeader = ({ title, icon, desc, ...props }) => {
			let margin = "2rem";
			return (
				<>
					{icon}
					<Typography
						style={{
							fontSize: "32px",
							fontWeight: 700,
							textAlign: "center",
							marginBottom: desc ? 0 : margin,
						}}
					>
						{title}
					</Typography>
					{desc ? (
						<Typography
							style={{ fontSize: "16px", fontWeight: 400, textAlign: "center", marginBottom: margin }}
						>
							{desc}
						</Typography>
					) : null}
				</>
			);
		};

		return (
			<div className={classes.root}>
				<PageHelmet metadata={{ title: `${this.titleText()} | StoreUp` }} />
				<div className={classes.container}>
					{LoginPage ? (
						<Fade in={LoginPage} unmountOnExit mountOnEnter>
							<div className={classes.innerContainer}>
								<PageContentHeader
									title="Login with Your StoreUp ID"
									icon={<IoMdMail className={classes.icon} />}
								/>
								<TextInput
									className={classes.textInput}
									variant="outlined"
									label="Email"
									placeholder="Your Email"
									type="email"
									value={email}
									onChange={this.handleChange("email")}
								/>
							</div>
						</Fade>
					) : null}
					{RegisterPage ? (
						<Fade in={RegisterPage} unmountOnExit mountOnEnter>
							<div className={classes.innerContainer}>
								<PageContentHeader
									title="Register an Account"
									icon={<BsPersonFill className={classes.icon} />}
								/>
								<TextInput
									className={classes.textInput}
									variant="outlined"
									label="Email"
									placeholder="Your Email"
									type="email"
									value={email}
									onChange={this.handleChange("email")}
								/>
							</div>
						</Fade>
					) : null}
					<input
						style={{
							width: 0.1,
							height: 0.1,
							backgroundColor: mainBgColor,
							position: "absolute",
							zIndex: -100,
						}}
						// maxLength={4}
						type="number"
						value={passcode}
						onChange={this.handleChange("passcode", 4)}
						ref={input => {
							this.passcodeInput = input;
						}}
					/>
					{PasscodePage ? (
						<Fade in={PasscodePage} unmountOnExit mountOnEnter>
							<div className={classes.innerContainer}>
								<PageContentHeader
									title="Enter StoreUp Passcode"
									desc={`Enter the 4-digit code we sent to ${this.state.email} to continue`}
									icon={<BiKey className={classes.icon} />}
								/>

								<div
									className={classes.passcodeInputBox}
									onClick={() => {
										this.passcodeInput.focus();
									}}
								>
									<div
										className={classes.passcodeInput}
										style={{ borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }}
									>
										<Typography className={classes.passcodeInputText}>{passcode[0]}</Typography>
									</div>
									<div className={classes.passcodeInput}>
										<Typography className={classes.passcodeInputText}>{passcode[1]}</Typography>
									</div>
									<div className={classes.passcodeInput}>
										<Typography className={classes.passcodeInputText}>{passcode[2]}</Typography>
									</div>
									<div
										className={classes.passcodeInput}
										style={{ borderTopRightRadius: 10, borderBottomRightRadius: 10 }}
									>
										<Typography className={classes.passcodeInputText}>{passcode[3]}</Typography>
									</div>
								</div>
							</div>
						</Fade>
					) : null}
					{BusinessPage ? (
						<Fade in={BusinessPage} unmountOnExit mountOnEnter>
							<div className={classes.innerContainer}>
								<PageContentHeader
									title="Enter Business Details"
									icon={<IoBusiness className={classes.icon} />}
								/>
								<TextInput
									className={classes.textInput}
									variant="outlined"
									label="Business name"
									placeholder="Your business name"
									value={bName}
									onChange={this.handleChange("bName")}
								/>
								<TextInput
									className={classes.textInput}
									variant="outlined"
									label="Phone"
									placeholder="Your phone number"
									value={bPhone}
									onChange={this.handleChange("bPhone")}
									startAdornment="+60"
								/>
							</div>
						</Fade>
					) : null}
					{SuccessPage ? (
						<Fade in={SuccessPage} unmountOnExit mountOnEnter>
							<div className={classes.innerContainer}>
								<PageContentHeader
									title="StoreUp Account Created"
									desc="Congratulations! Your account has been created."
									icon={<BsCheckCircleFill className={classes.icon} />}
								/>
							</div>
						</Fade>
					) : null}
					<Button className={classes.button} variant="contained" color="primary" onClick={this.handleSubmit}>
						{step === 5 ? "Download app" : "Continue"}
					</Button>
					{LoginPage ? (
						<Fade in={LoginPage} unmountOnExit mountOnEnter>
							<Typography
								className={classes.textButton}
								onClick={() => {
									this.setState({
										step: 2,
									});
								}}
							>
								Don’t have a StoreUp ID? Register here
							</Typography>
						</Fade>
					) : null}
					{RegisterPage ? (
						<Fade in={RegisterPage} unmountOnExit mountOnEnter>
							<Typography
								className={classes.textButton}
								onClick={() => {
									this.setState({
										step: 1,
									});
								}}
							>
								Already have a StoreUp ID? Login here
							</Typography>
						</Fade>
					) : null}
					{PasscodePage ? (
						<Fade in={PasscodePage} unmountOnExit mountOnEnter>
							<Typography className={classes.textButton} onClick={this.handleSendEmail}>
								Have not received your passcode? Resend here
							</Typography>
						</Fade>
					) : null}
				</div>
			</div>
		);
	}
}

const styles = theme => ({
	root: {
		alignSelf: "center",
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
		maxWidth: "500px",
		height: "100vh",
		minHeight: "600px",
		//padding: "5rem 1rem 0",
		padding: "0 1rem 0",
		position: "relative",
		backgroundColor: mainBgColor,
	},
	container: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
		height: "100%",
	},
	innerContainer: {
		display: "flex",
		flexDirection: "column",
		justifyContent: "center",
		alignItems: "center",
		width: "100%",
	},
	textInput: {
		marginBottom: "1.5rem",
		width: "100%",
	},
	passcodeInputBox: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		marginBottom: "1.5rem",
	},
	passcodeInput: {
		display: "flex",
		justifyContent: "center",
		alignItems: "center",
		border: `2px solid ${mainBgColor}`,
		height: "90px",
		width: "90px",
		backgroundColor: "white",
		[theme.breakpoints.down("xs")]: {
			height: "70px",
			width: "70px",
		},
	},
	passcodeInputText: {
		fontWeight: 700,
		fontSize: 50,
	},
	icon: {
		fontSize: "10rem",
		color: primary,
		marginBottom: "1rem",
	},
	textButton: {
		color: primary,
		fontSize: "14px",
		cursor: "pointer",
	},
	button: {
		height: "40px",
		width: "100%",
		marginBottom: "1.5rem",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	noti: state.noti,
	error: state.error,
});

export default connect(mapStateToProps, {
	sendUserEmail,
	loginUser,
	editStoreDetails,
	customNotification,
	setLoading,
})(withStyles(styles)(Home));
