import React, { Component } from "react";
import { connect } from "react-redux";
import { forgotPassword } from "../store/actions/authAction";
import { customNotification } from "../store/actions/notiAction";

import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import TitlePage from "../components/mini/TitlePage";
import PageHelmet from "../components/mini/PageHelmet";
import PasswordInput from "../components/mini/PasswordInput";
import isEmpty from "../utils/isEmpty";
import DoneOIcon from "@material-ui/icons/CheckCircleOutlineRounded";
import { Fade, Typography } from "@material-ui/core";
import { HighlightOffRounded } from "@material-ui/icons";
import { setLoading } from "../store/actions/loadingAction";

const styles = (theme) => ({
  root: {
    alignSelf: "center",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    maxWidth: "700px",
    height: "100vh",
    minHeight: "400px",
    padding: "5rem 1rem 0",
  },
  title: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "3rem",
    textAlign: "center",
    letterSpacing: "3px",
  },
  divider: {
    borderBottom: "3px solid #ab915d",
    width: "70%",
    margin: "0.2rem auto 0",
    marginBottom: "3rem",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    //maxWidth: "500px",
  },
  textField: {
    width: "100%",
    margin: "1rem 0",
  },
  button: {
    margin: "2rem 0",
    padding: "1rem 0",
  },
  doneSection: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    maxWidth: "700px",
    margin: "1rem 0",
    alignItems: "center",
  },
  doneIcon: {
    fontSize: "12rem",
    color: "#29a9c6",
    [theme.breakpoints.down("xs")]: {
      fontSize: "10rem",
    },
  },
  doneButton: {
    margin: "2rem 0",
    padding: "1rem 2rem",
  },
  doneTitle: {
    fontSize: "2.3rem",
    fontWeight: 500,
    textAlign: "center",
    marginTop: "1.5rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.6rem",
    },
  },
  doneDesc: {
    fontSize: "1.5rem",
    fontWeight: 300,
    textAlign: "center",
    marginTop: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
});

class ForgotPassword extends Component {
  state = {
    email: "",
    success: null,
    text: "",
  };

  componentDidUpdate(prevProps, prevState) {
    if (isEmpty(prevProps.noti.status) && !isEmpty(this.props.noti.status)) {
      if (this.props.noti.status === "success") {
        this.setState({
          success: true,
          text: this.props.noti.message,
        });
      }
    }
  }

  handlePage = (link) => () => {
    if (link.includes("http")) {
      window.open(link);
    } else {
      this.props.history.push(link);
    }
  };

  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = () => {
    let data = {
      email: this.state.email,
    };
    this.props.setLoading(true);
    this.props.forgotPassword(data);
  };

  render() {
    const { classes, noti } = this.props;
    const { email, text, success } = this.state;

    return (
      <div className={classes.root}>
        <PageHelmet metadata={{ title: "Forgot Password | StoreUp" }} />
        {!isEmpty(success) ? (
          success ? (
            <Fade in={success === true} unmountOnExit mountOnEnter>
              <div className={classes.doneSection}>
                <DoneOIcon className={classes.doneIcon} />
                <Typography className={classes.doneTitle}>
                  Email sent
                </Typography>

                <Typography className={classes.doneDesc}>{text}</Typography>
                {/* TODO: login button when dah ada webapp */}
              </div>
            </Fade>
          ) : (
            <Fade in={success === false} unmountOnExit mountOnEnter>
              <div className={classes.doneSection}>
                <HighlightOffRounded
                  className={classes.doneIcon}
                  style={{ color: "firebrick" }}
                />
                <Typography className={classes.doneTitle}>
                  Unauthorized
                </Typography>

                <Typography className={classes.doneDesc}>
                  Opps! your link is already expired. Please click the button
                  below to re-submit your change password link.
                </Typography>
                <Button
                  className={classes.doneButton}
                  variant="contained"
                  color="primary"
                  onClick={this.handlePage("/forgot-password")}
                >
                  Request password change
                </Button>
              </div>
            </Fade>
          )
        ) : (
          <>
            <TitlePage
              title="Forgot Password"
              fontSizeL="3rem"
              fontSizeS="2rem"
              marginBottom="1rem"
            />
            <div className={classes.content}>
              <TextField
                className={classes.textField}
                variant="outlined"
                label="Email"
                placeholder="Registered email"
                value={email}
                onChange={this.handleChange("email")}
              />

              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={this.handleSubmit}
              >
                Send link
              </Button>
            </div>
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  noti: state.noti,
  error: state.error,
});

export default connect(mapStateToProps, {
  forgotPassword,
  customNotification,
  setLoading,
})(withStyles(styles)(ForgotPassword));
