import React, { Component } from "react";
import { Helmet } from "react-helmet";
import isEmpty from "../utils/isEmpty";
import { connect } from "react-redux";
import { activateAccount } from "../store/actions/authAction";
import { setLoading } from "../store/actions/loadingAction";

import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import TitlePage from "../components/mini/TitlePage";
import { Fade } from "@material-ui/core";
import PageHelmet from "../components/mini/PageHelmet";
import DoneOIcon from "@material-ui/icons/CheckCircleOutlineRounded";
import ErrorOIcon from "@material-ui/icons/HighlightOffRounded";
import { errorColor, primary, secondary } from "../utils/ColorPicker";
import { CheckCircleRounded } from "@material-ui/icons";

const styles = theme => ({
	root: {
		alignSelf: "center",
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		width: "100%",
		maxWidth: "700px",
		minHeight: "400px",
		padding: "0 1.5rem",
		flex: 1,
		[theme.breakpoints.down("sm")]: {
			padding: "1rem 1.5rem 0",
			//	justifyContent: "flex-start",
		},
	},
	container: {
		width: "100%",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
	},
	title: {
		fontSize: "38px",
		textAlign: "center",
		fontWeight: 700,
		marginBottom: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "28px",
		},
	},
	divider: {
		borderBottom: "3px solid #ab915d",
		width: "70%",
		margin: "0.2rem auto 3rem",
	},
	content: {
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		width: "100%",
	},
	detail: {
		textAlign: "left",
		fontSize: "1.3rem",
		[theme.breakpoints.down("xs")]: {
			textAlign: "left",
		},
	},
	button: {
		height: "50px",
		borderRadius: "25px",
		marginTop: "1rem",
		width: "100%",
		maxWidth: "500px",
	},
	doneIcon: {
		fontSize: "10rem",
		color: primary,
		marginBottom: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "9rem",
		},
	},
	errorIcon: {
		fontSize: "12rem",
		color: errorColor,
		marginBottom: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "10rem",
		},
	},
});

class VerifyByEmail extends Component {
	state = {
		status: null,
	};

	componentDidMount() {
		this.props.setLoading(true);
		let search = window.location.href.split("?")[1];
		let urlParams = new URLSearchParams(search);
		let tkn = urlParams.get("token");

		setTimeout(() => {
			if (!isEmpty(tkn)) {
				this.props.activateAccount(tkn);
			} else {
				this.props.setLoading(false);
				this.setState({
					status: false,
				});
			}
		}, 1000);
	}

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.auth.message) && !isEmpty(this.props.auth.message)) {
			this.props.setLoading(false);
			this.props.auth.message === 200
				? this.setState({
						status: true,
				  })
				: this.setState({
						status: false,
				  });
		}
	}

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			this.props.history.push(link);
		}
	};

	render() {
		const { classes } = this.props;
		const { status } = this.state;
		const loading = this.props.loading.status;

		if (!isEmpty(status)) {
			return (
				<div className={classes.root}>
					<PageHelmet metadata={{ title: "Verify Email | StoreUp" }} />

					<Fade in={!loading}>
						<div className={classes.container}>
							{/* <TitlePage
                title={status ? "Your email has been verified" : "Error"}
                fontSizeL="3rem"
                fontSizeS="2rem"
              /> */}
							{status ? (
								<div className={classes.content}>
									<CheckCircleRounded className={classes.doneIcon} />
									<Typography className={classes.title}>
										Your email has been verified. You may continue using the app.
									</Typography>

									{/* <Button
                    className={classes.button}
                    variant="contained"
                    color="primary"
                    onClick={this.handlePage("/downloads")}
                  >
                    Download app
                  </Button> */}
								</div>
							) : (
								<div className={classes.content}>
									<ErrorOIcon className={classes.errorIcon} />
									<Typography className={classes.title}>Your account cannot be activated.</Typography>
									<br />
									<Typography className={classes.detail}>
										Please ensure that you have clicked the right link. You can also resend link in
										StoreUp app.
									</Typography>
									<br />
									<Typography className={classes.detail}>
										If problem still persists, please do not hesitate to reach us through email at
										support@mail.storeup.io.
									</Typography>
								</div>
							)}
						</div>
					</Fade>
				</div>
			);
		} else {
			return <div />;
		}
	}
}

const mapStateToProps = state => ({
	auth: state.auth,
	loading: state.loading,
	error: state.error,
});

export default connect(mapStateToProps, {
	activateAccount,
	setLoading,
})(withStyles(styles)(VerifyByEmail));
