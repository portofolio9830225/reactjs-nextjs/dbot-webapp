import React, { Component } from "react";
import { connect } from "react-redux";
import { customNotification } from "../store/actions/notiAction";

import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import TitlePage from "../components/mini/TitlePage";
import PageHelmet from "../components/mini/PageHelmet";
import PasswordInput from "../components/mini/PasswordInput";
import isEmpty from "../utils/isEmpty";
import DoneOIcon from "@material-ui/icons/CheckCircleOutlineRounded";
import { Fade, Typography, Collapse } from "@material-ui/core";
import { ChevronLeft, HighlightOffRounded } from "@material-ui/icons";
import { setLoading } from "../store/actions/loadingAction";
import {
  createTrackingBulkUpload,
  extractTrackingFromCsv,
} from "../store/actions/itemAction";
import { greydark, mainBgColor, secondaryDark } from "../utils/ColorPicker";

const styles = (theme) => ({
  root: {
    alignSelf: "center",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    //justifyContent: "center",
    width: "100%",
    maxWidth: "700px",
    //height: "100vh",
    minHeight: "400px",
    //padding: "0 1rem",
  },
  title: {
    fontFamily: "Poppins, sans-serif",
    fontSize: "3rem",
    textAlign: "center",
    letterSpacing: "3px",
  },
  divider: {
    borderBottom: "3px solid #ab915d",
    width: "70%",
    margin: "0.2rem auto 0",
    marginBottom: "3rem",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    position: "relative",
    //maxWidth: "500px",
  },
  uploadBox: {
    borderBottom: "1px solid gainsboro",
    padding: "1.5rem 1rem",
    boxSizing: "border-box",
    // backgroundColor: mainBgColor,
    [theme.breakpoints.down("xs")]: {
      padding: "0.5rem 1rem",
    },
  },
  uploadDesc: {
    fontSize: "1.1rem",
    marginBottom: "1rem",
  },
  uploadActionRow: {
    display: "flex",
    flexDirection: "row",
    //justifyContent: "space-between",
    alignItems: "center",
    marginBottom: "1.5rem",
  },
  trackingList: {
    padding: "2rem 0",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  trackingBox: {
    width: "100%",
    padding: "1.5rem 1rem",
    borderRadius: 10,
    boxSizing: "border-box",
    marginBottom: "2rem",
    boxShadow: "inset 2px 2px 4px #d6d6e3, inset -2px -2px 5px #ffffff",
    // boxShadow:
    //   "-5px -5px 5px rgba(255, 255, 255, 0.5), 5px 5px 10px rgba(174, 174, 192, 0.5), inset -2px -2px 4px rgba(0, 0, 0, 0.1), inset 2px 2px 4px #FFFFFF",
  },
  button: {
    // margin: "2rem 0",
    padding: "1rem 0",
    width: "100%",
  },
  doneSection: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    maxWidth: "700px",
    margin: "1rem 0",
    alignItems: "center",
  },
  doneIcon: {
    fontSize: "12rem",
    color: "#29a9c6",
    [theme.breakpoints.down("xs")]: {
      fontSize: "10rem",
    },
  },
  doneButton: {
    margin: "2rem 0",
    padding: "1rem 2rem",
  },
  doneTitle: {
    fontSize: "2.3rem",
    fontWeight: 500,
    textAlign: "center",
    marginTop: "1.5rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.6rem",
    },
  },
  doneDesc: {
    fontSize: "1.5rem",
    fontWeight: 300,
    textAlign: "center",
    marginTop: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
});

class TrackingBulkUpload extends Component {
  constructor(props) {
    super(props);
    this.fileInputRef = React.createRef();
  }
  state = {
    token: "",
    file: {},
    fileName: "",
    success: null,
  };

  componentDidMount() {
    let params = new URLSearchParams(window.location.search);
    let token = params.get("token");
    if (!isEmpty(token)) {
      this.setState({
        token,
      });
    } else {
      this.props.customNotification("Invalid link", "error");
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (isEmpty(prevProps.noti.status) && !isEmpty(this.props.noti.status)) {
      this.setState({
        success: this.props.noti.status === "success",
        text: this.props.noti.message,
      });
    }
  }

  handleBack = () => {
    this.setState({
      file: {},
      fileName: "",
    });
    this.fileInputRef.current.value = null;
    this.props.extractTrackingFromCsv();
  };

  handlePage = (link) => () => {
    if (link.includes("http")) {
      window.open(link);
    } else {
      this.props.history.push(link);
    }
  };

  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  fileUpload = () => {
    document.getElementById("fileUpload").click();
  };

  onSelectFile = (e) => {
    this.props.setLoading(true);
    this.setState(
      {
        fileName: e.target.files[0].name,
        file: e.target.files[0],
      },
      () => {
        let data = new FormData();
        data.append("sheet", this.state.file);

        this.props.extractTrackingFromCsv(data, this.state.token);
      }
    );
  };

  handleSubmit = () => {
    this.props.createTrackingBulkUpload(
      this.props.item.bulkTrackingData,
      this.state.token
    );
  };

  render() {
    const { classes, noti } = this.props;
    const { fileName, text, success } = this.state;
    const { bulkTrackingData } = this.props.item;

    const TitleDesc = ({ title, desc, noWrap }) => {
      return (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            marginBottom: "1rem",
          }}
        >
          <Typography
            style={{ fontWeight: 300, fontSize: "0.9rem", color: greydark }}
          >
            {title}
          </Typography>
          <Typography
            noWrap={noWrap}
            style={{ fontWeight: 500, fontSize: "1rem" }}
          >
            {desc}
          </Typography>
        </div>
      );
    };

    const TrackingBox = ({ id, courier, trackNum, name }) => {
      return (
        <div className={classes.trackingBox}>
          {/* <TitleDesc title="Order ID" desc={id} /> */}
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <div style={{ width: "50%" }}>
              <TitleDesc title="Order ID" desc={id} />
            </div>
            <div style={{ width: "50%" }}>
              <TitleDesc title="Courier" desc={courier} />
            </div>
          </div>
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <div style={{ width: "50%" }}>
              <TitleDesc title="Customer name" desc={name} noWrap />
            </div>
            <div style={{ width: "50%" }}>
              <TitleDesc title="Tracking number" desc={trackNum} />
            </div>
          </div>
        </div>
      );
    };
    return (
      <div className={classes.root}>
        <PageHelmet
          metadata={{ title: "Tracking number bulk upload | StoreUp" }}
        />
        {isEmpty(success) ? (
          <div className={classes.content}>
            <div
              style={{
                position: "sticky",
                top: 75,
                paddingTop: 0,
                //paddingBottom: "0.5rem",
                backgroundColor: mainBgColor,
              }}
            >
              {isEmpty(fileName) && (
                <TitlePage
                  title="Tracking number bulk upload"
                  fontSizeL="3rem"
                  fontSizeS="2rem"
                  marginBottom="1rem"
                  marginLeft="1rem"
                />
              )}

              <div className={classes.uploadBox}>
                <Collapse in={isEmpty(fileName)}>
                  <ol style={{ paddingLeft: "1rem", marginBottom: "2rem" }}>
                    <li>
                      <Typography className={classes.uploadDesc}>
                        Download XSLX template by clicking{" "}
                        <span
                          style={{
                            textDecoration: "underline",
                            cursor: "pointer",
                            color: secondaryDark,
                          }}
                          onClick={this.handlePage(
                            "https://dbot-storage.s3.ap-southeast-1.amazonaws.com/template/tracking-bulk-template.xlsx"
                          )}
                        >
                          here
                        </span>
                      </Typography>
                    </li>
                    <li>
                      <Typography className={classes.uploadDesc}>
                        Fill up the sheet accorrding to the header columns. Each
                        row represent one order
                      </Typography>
                    </li>
                    <li>
                      <Typography className={classes.uploadDesc}>
                        Once done, save the file as{" "}
                        <span style={{ fontWeight: 600 }}>
                          Comma-seperated values file (.csv)
                        </span>{" "}
                        and upload the saved file by clicking the button below.
                      </Typography>
                    </li>
                  </ol>
                </Collapse>
                <Collapse in={!isEmpty(fileName)}>
                  <ChevronLeft
                    style={{
                      color: greydark,
                      fontSize: "2.5rem",
                      paddingBottom: "1rem",
                      cursor: "pointer",
                    }}
                    onClick={this.handleBack}
                  />
                </Collapse>
                <div className={classes.uploadActionRow}>
                  <div>
                    <Typography
                      style={{ color: greydark, marginBottom: "0.2rem" }}
                    >
                      CSV file:
                    </Typography>
                    <Typography style={{ fontWeight: 700, fontSize: "1.1rem" }}>
                      {!isEmpty(fileName) ? fileName : "No file chosen"}
                    </Typography>
                  </div>
                  <input
                    ref={this.fileInputRef}
                    type="file"
                    accept=".csv"
                    id="fileUpload"
                    hidden
                    onChange={this.onSelectFile}
                  />
                  <Button
                    variant="outlined"
                    color="primary"
                    style={{ marginLeft: "2rem" }}
                    onClick={this.fileUpload}
                  >
                    {!isEmpty(fileName) ? "Change" : "Choose"}
                  </Button>
                </div>
                <Collapse in={!isEmpty(fileName)}>
                  <Button
                    className={classes.button}
                    variant="contained"
                    color="primary"
                    onClick={this.handleSubmit}
                  >
                    Submit CSV
                  </Button>
                </Collapse>
              </div>
            </div>
            {!isEmpty(bulkTrackingData) && (
              <div className={classes.trackingList}>
                {bulkTrackingData.orders.map((e, i) => {
                  return (
                    <TrackingBox
                      id={e.order_id}
                      name={e.customer_name}
                      courier={e.courier}
                      trackNum={e.tracking_no}
                    />
                  );
                })}
              </div>
            )}
          </div>
        ) : success ? (
          <Fade in={success === true} unmountOnExit mountOnEnter>
            <div className={classes.doneSection}>
              <DoneOIcon className={classes.doneIcon} />
              <Typography className={classes.doneTitle}>Success!</Typography>

              <Typography className={classes.doneDesc}>{text}</Typography>
            </div>
          </Fade>
        ) : (
          <Fade in={success === false} unmountOnExit mountOnEnter>
            <div className={classes.doneSection}>
              <HighlightOffRounded
                className={classes.doneIcon}
                style={{ color: "firebrick" }}
              />
              <Typography className={classes.doneTitle}>
                Unauthorized
              </Typography>

              <Typography className={classes.doneDesc}>
                Opps! your link is already expired. Please request another link
                from StoreUp mobile app.
              </Typography>
              {/* <Button
                  className={classes.doneButton}
                  variant="contained"
                  color="primary"
                  onClick={this.handlePage("/forgot-password")}
                >
                  Request password change
                </Button> */}
            </div>
          </Fade>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  item: state.item,
  noti: state.noti,
  error: state.error,
});

export default connect(mapStateToProps, {
  extractTrackingFromCsv,
  createTrackingBulkUpload,
  customNotification,
  setLoading,
})(withStyles(styles)(TrackingBulkUpload));
