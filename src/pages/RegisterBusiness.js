import React, { Component } from "react";
import withWidth from "@material-ui/core/withWidth";
import isEmpty from "../utils/isEmpty";
import { connect } from "react-redux";
import { registerBusiness } from "../store/actions/authAction";

import {
  customNotification,
  clearNotification,
} from "../store/actions/notiAction";
import { setLoading } from "../store/actions/loadingAction";
import DoneOIcon from "@material-ui/icons/CheckCircleOutlineRounded";

import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import content from "../json/registerBusiness.json";
import navbar from "../json/mini/navbar.json";

import { Fade, InputAdornment, withMobileDialog } from "@material-ui/core";

import PasswordInput from "../components/mini/PasswordInput";
import NavBar from "../components/mini/NavBar";
import PageHelmet from "../components/mini/PageHelmet";
import { isMobile } from "react-device-detect";
import TextInput from "../components/mini/TextInput";

const styles = (theme) => ({
  root: {
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    height: "100vh",
    minHeight: "950px",
    [theme.breakpoints.down("xs")]: {
      height: "auto",
      minHeight: "auto",
    },
  },
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",

    width: "100%",
    height: "100%",
    position: "relative",
    [theme.breakpoints.down("xs")]: {
      height: "auto",
      minHeight: "auto",
    },
  },
  authModalContainer: {
    display: "flex",
    flexDirection: "column",
    width: "90%",
    marginTop: "2rem",
    maxWidth: "700px",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
    },
  },
  authContent: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    maxWidth: "700px",
  },
  outerContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "50%",
    height: "100%",
    minHeight: "800px",
    [theme.breakpoints.down("md")]: {
      boxSizing: "border-box",
      width: "100%",
      minHeight: "auto",
      paddingBottom: "15rem",
    },
  },
  content: {
    boxSizing: "border-box",
    marginTop: "3rem",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "100%",
    height: "100%",
    padding: "0 2rem",
    [theme.breakpoints.down("xs")]: {
      padding: "0 1.5rem",
    },
  },
  authTitle: {
    zIndex: 10,
    fontWeight: "bold",
    margin: "1rem 0",
    fontSize: "3.5rem",
    textAlign: "left",
    width: "100%",
    [theme.breakpoints.down("xs")]: {
      marginBottom: "1rem",
      fontSize: "2.5rem",
    },
  },
  authDetail: {
    fontWeight: 300,
    fontSize: "1.2rem",
    textAlign: "left",
    width: "100%",
    marginBottom: "2rem",
    [theme.breakpoints.down("xs")]: {
      marginBottom: "1rem",
      fontSize: "0.9rem",
    },
  },
  title: {
    fontSize: "1.8rem",
    marginBottom: "2.5rem",
    fontWeight: "bold",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.6rem",
    },
  },
  divider: {
    borderBottom: "3px solid #ab915d",
    width: "70%",
    margin: "0.2rem auto 0",
  },

  section: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    maxWidth: "700px",
    margin: "1rem 0",
  },
  doneSection: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    maxWidth: "700px",
    margin: "1rem 0",
    alignItems: "center",
  },

  textField: {
    width: "100%",
    margin: "1rem 0",
  },
  button: {
    width: "100%",
    margin: "1rem 0",
    padding: "1rem 0",
    maxWidth: "700px",
  },

  link: {
    fontSize: "1rem",
    padding: "1rem 0",
    alignSelf: "center",
    cursor: "pointer",
    color: "#888",
    textDecoration: "underline",
    transition: "color 0.3s",
    "&:hover": {
      color: "#000",
      textDecoration: "none",
    },
    [theme.breakpoints.down("xs")]: {
      alignSelf: "flex-start",
    },
  },
  modalContent: {
    display: "flex",
    justifyContent: "center",
    width: "90%",
    padding: "3rem",
    [theme.breakpoints.down("xs")]: {
      padding: "1rem",
    },
  },
  dialogPaper: {
    backgroundColor: "rgba(0,0,0,0)",
    boxShadow: "none",
  },

  modalButtonContainer: {
    position: "fixed",
    bottom: 0,
    display: "flex",
    justifyContent: "center",
    margin: "2rem 0",
  },
  modalButtonO: {
    color: "white",
    borderColor: "white",
    margin: "0 0.5rem",
  },
  modalButtonC: {
    color: "black",
    backgroundColor: "white",
    margin: "0 0.5rem",
  },
  paperScrollBody: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    boxSizing: "border-box",
    margin: "auto",
    padding: "2rem 0",
    minHeight: "100vh",
    minWidth: "100vw",
    overflow: "hidden",
  },
  paperScrollPaperAuth: {
    display: "flex",
    flexDirection: "column",

    alignItems: "center",
    boxSizing: "border-box",
    margin: "auto",
    padding: "1rem 1.5rem",
    minHeight: "100vh",
    minWidth: "100vw",
    overflowX: "hidden",
  },
  addIcon: {
    fontSize: "2.5rem",
    color: "#888",
  },
  addCaption: {
    width: "80%",
    textAlign: "center",
    fontSize: "1rem",
    color: "#888",
  },
  businessHeader: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },
  businessTimeContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },
  storeHeaderImage: {
    width: "100%",
    height: "120px",
    border: "1px dashed gainsboro",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
    marginBottom: "2rem",
    [theme.breakpoints.only("xs")]: {
      height: "100px",
    },
  },
  businessHeaderImage: {
    overflow: "hidden",
    width: "150px",
    height: "150px",
    borderRadius: "50%",
    border: "1px solid gainsboro",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    cursor: "pointer",
    [theme.breakpoints.only("sm")]: {
      width: "200px",
      height: "200px",
    },
  },
  businessHeaderInput: {
    width: "65%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-around",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      marginTop: "2rem",
    },
  },
  ssmFileBox: {
    margin: "1rem 0",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    border: "0.5px solid gainsboro",
    borderRadius: 7,
    padding: "1rem 1.5rem",
    boxSizing: "border-radius",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },

  ssmFileTitle: {
    fontSize: "1.1rem",
    color: "dimgrey",
    letterSpacing: "0.7px",
  },
  ssmFileName: {
    fontSize: "1rem",
    marginTop: "0.1rem",
  },
  ssmFileContent: {
    display: "flex",
    flexDirection: "column",
    [theme.breakpoints.down("xs")]: {
      marginBottom: "2rem",
    },
  },
  bankErrorBox: {
    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "crimson",
    borderRadius: 5,
    padding: "0.7rem",
    marginBottom: "1rem",
  },
  bankSuccessBox: {
    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "#29a9c6",
    borderRadius: 5,
    padding: "0.7rem",
    marginBottom: "1rem",
  },
  bankPendingBox: {
    width: "100%",
    boxSizing: "border-box",
    backgroundColor: "dimgrey",
    borderRadius: 5,
    padding: "0.7rem",
    marginBottom: "1rem",
  },
  bankText: {
    color: "white",
    fontSize: "1rem",
  },
  loadingDialog: {
    backgroundColor: "rgba(0,0,0,0)",
    boxShadow: "none",
  },
  closeIcon: {
    alignSelf: "flex-end",
    color: "dimgrey",
    padding: "2rem",
    fontSize: "2.5rem",
    marginBottom: "1rem",
    cursor: "pointer",
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
      padding: "0.5rem",
    },
  },
  authContainer: {
    width: "100%",
    height: "600px",
    padding: "3rem 0 0",
    boxSizing: "border-box",
    justifyContent: "space-between",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
      padding: "1.5rem 0",
      height: "100%",
    },
  },
  authContentContainer: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    height: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  authBox: {
    cursor: "pointer",
    width: "47%",
    height: "100%",
    maxHeight: "500px",
    boxSizing: "border-box",
    border: "1px solid gainsboro",
    borderRadius: "10px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-evenly",
    [theme.breakpoints.down("xs")]: {
      width: "100%",
      height: "350px",
    },
  },
  authBoxImgContainer: {
    height: "80%",
    width: "85%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    boxSizing: "border-box",
    padding: "0.5rem",
    [theme.breakpoints.down("xs")]: {
      height: "75%",
    },
  },
  image: {
    maxWidth: "100%",
    maxHeight: "100%",
  },
  authBoxTextContainer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    height: "20%",
    [theme.breakpoints.down("xs")]: {
      height: "25%",
    },
  },
  authBoxTitle: {
    fontWeight: "bold",
    fontSize: "2rem",
    marginBottom: "0.5rem",
    letterSpacing: "0.7px",
    [theme.breakpoints.down("xs")]: {
      fontSize: "2rem",
      marginBottom: "0.2rem",
    },
  },
  authBoxDesc: {
    textAlign: "center",
    fontWeight: 300,
    fontSize: "1.2rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
    },
  },
  authDivider: {
    height: "50%",
    borderRight: "1px solid darkgrey",
    [theme.breakpoints.down("xs")]: {
      height: "auto",
      borderRight: "none",
      width: "70%",
      borderBottom: "1px solid gainsboro",
      margin: "2rem 0",
    },
  },
  stepper: {
    width: "100%",
    maxWidth: "700px",
    marginBottom: "2.5rem",
    [theme.breakpoints.down("xs")]: {
      marginBottom: 0,
    },
  },
  leftContent: {
    backgroundColor: "#252525",
    display: "flex",
    flexDirection: "column",
    width: "50%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  leftContentImgContainer: {
    height: "50%",
    width: "60%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  contentTitle: {
    textAlign: "left",
    fontWeight: 300,
    width: "100%",
    fontSize: "1.5rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1rem",
      marginBottom: "1rem",
    },
  },
  stepButtonContainer: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    maxWidth: "700px",
    marginTop: "2rem",
  },
  stepButtonBack: {
    padding: "1rem 2.5rem 1rem 2rem",
    [theme.breakpoints.down("xs")]: {
      padding: "0.7rem 2.3rem 0.7rem 1.5rem",
    },
  },
  stepButtonNext: {
    padding: "1rem 2rem 1rem 2.5rem",
    [theme.breakpoints.down("xs")]: {
      padding: "0.7rem 1.5rem 0.7rem 2.3rem",
    },
  },
  iconActive: {
    transition: "all 0.3s",
    fontSize: "2.2rem",
    color: "dimgrey",
  },
  iconCompleted: {
    transition: "all 0.3s",
    fontSize: "2.2rem",
    color: "#007a95",
  },
  iconDisabled: {
    transition: "all 0.3s",
    fontSize: "1.7rem",
    color: "gainsboro",
  },
  connectorActive: {
    "& $connectorLine": {
      borderColor: "gainsboro",
    },
  },
  connectorCompleted: {
    "& $connectorLine": {
      borderColor: "#29a9c6",
    },
  },
  connectorDisabled: {
    "& $connectorLine": {
      borderColor: "gainsboro",
    },
  },
  connectorLine: {
    transition: theme.transitions.create("border-color"),
    borderWidth: "2px",
  },
  doneIcon: {
    fontSize: "12rem",
    color: "#29a9c6",
    [theme.breakpoints.down("xs")]: {
      fontSize: "10rem",
    },
  },
  doneButton: {
    margin: "2rem 0",
    padding: "1rem 2rem",
  },
  doneTitle: {
    fontSize: "2.3rem",
    fontWeight: 500,
    textAlign: "center",
    marginTop: "1.5rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.6rem",
    },
  },
  doneDesc: {
    fontSize: "1.5rem",
    fontWeight: 300,
    textAlign: "center",
    marginTop: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
  registerButton: {
    height: 50,
    //borderRadius: 25,
    width: "100%",
    maxWidth: "700px",
    marginTop: "3rem",
  },
});

class BusinessSignUp extends Component {
  state = {
    data: content,
    active: 1,
    step: 1,
    uName: "",
    uEmail: "",
    uPhone: "",
    uPass: "",
    bName: "",
  };
  componentDidMount() {
    let search = window.location.href.split("?")[1];
    let urlParams = new URLSearchParams(search);
    let phone = urlParams.get("phone");
    let name = urlParams.get("name");
    this.setState({
      uPhone: phone || "",
      uName: name || "",
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.noti.status === "success" &&
      isEmpty(prevProps.noti.status)
    ) {
      this.setState({
        step: 5,
      });
    }
  }

  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handlePage = (page) => () => {
    this.props.history.push(page);
  };

  handleRedirect = (page) => () => {
    window.location.href = page;
  };

  handleSubmit = () => {
    let { uName, uEmail, uPhone, uPass, bName } = this.state;

    let data = {
      user_name: uName,
      user_email: uEmail,
      user_phone: uPhone,
      password: uPass,
      business_name: bName,
    };
    this.props.registerBusiness(data, this.props.auth.token);
  };

  render() {
    const { width, classes, fullScreen } = this.props;
    const { data, active, uName, uEmail, uPhone, uPass, bName, step } =
      this.state;

    return (
      <div className={classes.root}>
        <PageHelmet metadata={data.metadata} />

        <div className={classes.container}>
          {/*********************** After sign in START *************************/}

          <Fade in={active === 1} unmountOnExit>
            <div className={classes.outerContainer}>
              <NavBar homeLogo={navbar.logo} />
              <div className={classes.content}>
                {step === 1 && (
                  <Fade in={step === 1} unmountOnExit mountOnEnter>
                    <div className={classes.section}>
                      <Typography
                        className={classes.title}
                        style={{ marginBottom: 0 }}
                      >
                        Glad to see you here!
                      </Typography>
                      <Typography className={classes.title}>
                        Let's set up your account
                      </Typography>
                      <TextInput
                        className={classes.textField}
                        variant="outlined"
                        label="Email"
                        placeholder="Your Email"
                        type="email"
                        value={uEmail}
                        onChange={this.handleChange("uEmail")}
                        errorKey="user_email"
                      />
                      <TextInput
                        className={classes.textField}
                        variant="outlined"
                        label="Full Name"
                        placeholder="Full name"
                        value={uName}
                        onChange={this.handleChange("uName")}
                        errorKey="user_name"
                      />
                      <TextInput
                        className={classes.textField}
                        variant="outlined"
                        label="Phone number"
                        placeholder="Phone number"
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              +60
                            </InputAdornment>
                          ),
                        }}
                        value={uPhone}
                        onChange={this.handleChange("uPhone")}
                        errorKey="user_phone"
                      />
                      <PasswordInput
                        className={classes.textField}
                        type="password"
                        variant="outlined"
                        label="Password"
                        value={uPass}
                        onChange={this.handleChange("uPass")}
                        errorKey="password"
                      />
                      <TextInput
                        className={classes.textField}
                        variant="outlined"
                        label="Business name"
                        placeholder="will be shown in your online store"
                        value={bName}
                        onChange={this.handleChange("bName")}
                        errorKey="business_name"
                      />
                    </div>
                  </Fade>
                )}

                {step === 5 && (
                  <Fade in={step === 5} unmountOnExit mountOnEnter>
                    <div className={classes.doneSection}>
                      <DoneOIcon className={classes.doneIcon} />
                      {!isEmpty(data.success) && (
                        <Typography className={classes.doneTitle}>
                          {data.success.title}
                        </Typography>
                      )}
                      {!isEmpty(data.success) && (
                        <Typography className={classes.doneDesc}>
                          {data.success.desc}
                        </Typography>
                      )}
                    </div>
                  </Fade>
                )}
                <Fade in={step !== 5}>
                  <Button
                    disableRipple
                    className={classes.registerButton}
                    variant="contained"
                    color="primary"
                    onClick={this.handleSubmit}
                  >
                    Register
                  </Button>
                </Fade>
              </div>
            </div>
          </Fade>

          {/*********************** After sign in END *************************/}
          {(width === "xl" || width === "lg") && !isEmpty(data.bigImage) && (
            <div
              className={classes.leftContent}
              id="RegisterBusiness"
              style={{
                backgroundImage: `linear-gradient(
            rgba(15, 15, 15, 0),
            rgba(15, 15, 15, 0)
          ),url("${data.bigImage}")`,
              }}
            ></div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  loading: state.loading,
  noti: state.noti,
  error: state.error,
});

export default connect(mapStateToProps, {
  registerBusiness,
  customNotification,
  clearNotification,
  setLoading,
})(
  withStyles(styles)(
    withMobileDialog({ breakpoint: "lg" })(withWidth()(BusinessSignUp))
  )
);
