import React, { Component } from "react";
import { connect } from "react-redux";
import { resetPassword } from "../store/actions/authAction";
import { customNotification } from "../store/actions/notiAction";

import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TitlePage from "../components/mini/TitlePage";
import PageHelmet from "../components/mini/PageHelmet";
import isEmpty from "../utils/isEmpty";
import DoneOIcon from "@material-ui/icons/CheckCircleOutlineRounded";
import { Fade, Typography } from "@material-ui/core";
import { CheckCircleRounded, HighlightOffRounded } from "@material-ui/icons";
import { BiKey } from "react-icons/bi";
import { setLoading } from "../store/actions/loadingAction";
import TextInput from "../components/mini/TextInput";
import { mainBgColor, primary } from "../utils/ColorPicker";
import PasswordInput from "../components/mini/PasswordInput";

class ResetPassword extends Component {
	state = {
		pass1: "",
		success: null,
	};

	componentDidUpdate(prevProps, prevState) {
		if (isEmpty(prevProps.noti.status) && !isEmpty(this.props.noti.status)) {
			if (this.props.noti.status === "success") {
				this.setState({
					success: true,
				});
			} else {
				if (this.props.noti.redirect === "unauthorized") {
					this.setState({
						success: false,
					});
				}
			}
		}
	}

	handlePage = link => () => {
		if (link.includes("http")) {
			window.open(link);
		} else {
			this.props.history.push(link);
		}
	};

	handleChange = name => event => {
		this.setState({
			[name]: event.target.value,
		});
	};

	handleSubmit = () => {
		this.props.setLoading(true);
		let search = window.location.href.split("?")[1];
		let urlParams = new URLSearchParams(search);
		let tkn = urlParams.get("token");

		let data = {
			new_password: this.state.pass1,
		};

		this.props.resetPassword(data, tkn);
	};

	render() {
		const { classes } = this.props;
		const { pass1, success } = this.state;

		const PageContentHeader = ({ title, icon, desc, ...props }) => {
			let margin = "2rem";
			return (
				<>
					{icon}
					<Typography
						style={{
							fontSize: "28px",
							fontWeight: 700,
							textAlign: "center",
							marginBottom: desc ? 0 : margin,
						}}
					>
						{title}
					</Typography>
					{desc ? (
						<Typography
							style={{ fontSize: "16px", fontWeight: 400, textAlign: "center", marginBottom: margin }}
						>
							{desc}
						</Typography>
					) : null}
				</>
			);
		};

		return (
			<div className={classes.root}>
				<PageHelmet metadata={{ title: "Set New Password | StoreUp" }} />
				{!isEmpty(success) ? (
					success ? (
						<Fade in={success === true} unmountOnExit mountOnEnter>
							<div className={classes.doneSection}>
								<CheckCircleRounded className={classes.doneIcon} />
								<Typography className={classes.doneTitle}>Password updated</Typography>

								<Typography className={classes.doneDesc}>
									Your password has been updated. You can now login using your new password.
								</Typography>
								{/* TODO: login button when dah ada webapp */}
							</div>
						</Fade>
					) : (
						<Fade in={success === false} unmountOnExit mountOnEnter>
							<div className={classes.doneSection}>
								<HighlightOffRounded className={classes.doneIcon} style={{ color: "firebrick" }} />
								<Typography className={classes.doneTitle}>Unauthorized</Typography>

								<Typography className={classes.doneDesc}>
									Opps! your link is already expired. Please click the button below to re-submit your
									change password link.
								</Typography>
								<Button
									className={classes.doneButton}
									variant="contained"
									color="primary"
									onClick={this.handlePage("/forgot-password")}
								>
									Request password change
								</Button>
							</div>
						</Fade>
					)
				) : (
					<>
						<PageContentHeader
							title="Reset Your StoreUp ID Password"
							marginBottom="1rem"
							icon={<BiKey className={classes.icon} />}
						/>
						<Typography className={classes.subTittle}>
							Tell us your email address and we’ll send you an email with a link to reset your password.
						</Typography>
						<div className={classes.content}>
							<PasswordInput
								className={classes.textField}
								variant="outlined"
								label="New Password"
								value={pass1}
								onChange={this.handleChange("pass1")}
							/>
							<Button
								className={classes.button}
								variant="contained"
								color="primary"
								onClick={this.handleSubmit}
							>
								submit
							</Button>
						</div>
					</>
				)}
			</div>
		);
	}
}

const styles = theme => ({
	root: {
		alignSelf: "center",
		boxSizing: "border-box",
		display: "flex",
		flexDirection: "column",
		alignItems: "center",
		justifyContent: "center",
		width: "100%",
		maxWidth: "500px",
		height: "100vh",
		minHeight: "600px",
		position: "relative",
		backgroundColor: mainBgColor,
		flex: 1,
		paddingBottom: "5rem",
	},
	title: {
		fontSize: "3rem",
		textAlign: "center",
		letterSpacing: "3px",
	},
	subTittle: {
		fontSize: "1rem",
		padding: "0rem 1rem",
		textAlign: "center",
	},
	divider: {
		borderBottom: "3px solid #ab915d",
		width: "70%",
		margin: "0.2rem auto 0",
		marginBottom: "3rem",
	},
	content: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		//maxWidth: "500px",
	},
	button: {
		margin: "2rem 0",
		padding: "1rem 0",
	},
	icon: {
		fontSize: "10rem",
		color: primary,
		marginBottom: "1rem",
	},
	doneSection: {
		display: "flex",
		flexDirection: "column",
		width: "100%",
		maxWidth: "700px",
		margin: "1rem 0",
		alignItems: "center",
	},
	doneIcon: {
		fontSize: "10rem",
		color: primary,
		[theme.breakpoints.down("xs")]: {
			fontSize: "9rem",
		},
	},
	doneButton: {
		margin: "2rem 0",
		padding: "1rem 2rem",
	},
	doneTitle: {
		fontSize: "38px",
		fontWeight: 700,
		textAlign: "center",
		marginTop: "1.5rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "28px",
		},
	},
	doneDesc: {
		fontSize: "20px",
		fontWeight: 300,
		textAlign: "center",
		marginTop: "1rem",
		[theme.breakpoints.down("xs")]: {
			fontSize: "18px",
		},
	},
	textInput: {
		marginBottom: "1rem",
		width: "100%",
		marginTop: "3rem",
	},
	textField: {
		width: "100%",
		margin: "1rem 0",
	},
	textButton: {
		color: primary,
		fontSize: "14px",
		cursor: "pointer",
		// marginBottom: "2rem",
	},
	textBlack: {
		color: "#000",
		fontSize: "14px",
		margin: "0 0.5rem",
	},

	footer: {
		display: "flex",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
		margin: "1rem 0",
	},
});

const mapStateToProps = state => ({
	auth: state.auth,
	noti: state.noti,
	error: state.error,
});

export default connect(mapStateToProps, {
	resetPassword,
	customNotification,
	setLoading,
})(withStyles(styles)(ResetPassword));
