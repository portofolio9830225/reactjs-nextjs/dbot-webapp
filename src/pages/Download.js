import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import withWidth from "@material-ui/core/withWidth";

import { Typography, Tabs, Tab } from "@material-ui/core";
import isEmpty from "../utils/isEmpty";
import FadeBox from "../components/mini/FadeBox";
import content from "../json/download.json";
import PageHelmet from "../components/mini/PageHelmet";

const styles = (theme) => ({
  root: {
    position: "relative",
    width: "100%",
    height: "auto",
    boxSizing: "border-box",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    //alignItems: "center",
    padding: "3rem 0",
    // [theme.breakpoints.down("xs")]: {
    //   padding: "2rem 0",
    // },
  },
  content: {
    zIndex: 10,
    display: "flex",
    flexDirection: "column",
    width: "95%",
    maxWidth: "1400px",
    alignSelf: "center",
  },
  title: {
    zIndex: 10,
    fontWeight: "bold",
    marginBottom: "3rem",
    fontSize: "3.5rem",
    [theme.breakpoints.down("xs")]: {
      marginBottom: "1rem",
      fontSize: "2.5rem",
      textAlign: "left",
    },
  },
  tabText: {
    textTransform: "none",
    fontSize: "1.3rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "1.1rem",
    },
  },
  typeBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "95%",
    maxWidth: "500px",
  },
  typeBoxImgContainer: {
    width: "50%",
  },
  typeBoxImg: {
    width: "100%",
  },
  typeBoxText: {
    fontWeight: 300,
    textAlign: "center",
    fontSize: "1.3rem",
    marginBottom: "1rem",
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.9rem",
    },
  },
  typeBoxButton: {
    width: "100%",
  },
});

class DownloadSection extends Component {
  state = {
    active: null,
  };

  componentDidMount() {
    let params = new URLSearchParams(window.location.search);
    let section = params.get("app");
    const found = content.download.find((e) => e.app_name === section);

    this.setState({
      active: !isEmpty(found) ? section : content.download[0].app_name,
    });
  }

  handleActive = (event, value) => {
    this.setState({ active: value });
  };

  render() {
    const { width, classes } = this.props;
    const { color, title, download, metadata } = content;
    const { active } = this.state;

    return (
      <div className={classes.root}>
        <PageHelmet metadata={metadata} />
        {/* <SectionBackground color={color} horizontal="full" vertical="full" /> */}
        <div className={classes.content}>
          <Typography className={classes.title}>{title}</Typography>

          {/* <Tabs
            disableRipple={true}
            value={active}
            onChange={this.handleActive}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="off"
          >
            {!isEmpty(download) &&
              download.map((e, i) => (
                <Tab
                  disableRipple={true}
                  key={i}
                  value={e.app_name}
                  label={
                    <Typography className={classes.tabText}>
                      {e.title}
                    </Typography>
                  }
                />
              ))}
          </Tabs> */}

          {!isEmpty(download) &&
            download.map((e, i) => {
              return (
                <FadeBox
                  key={i}
                  active={active}
                  val={e.app_name}
                  title={e.title}
                  arr={e.device}
                />
              );
            })}
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(withWidth()(DownloadSection));
