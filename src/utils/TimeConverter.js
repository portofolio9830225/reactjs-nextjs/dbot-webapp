const TimeConverter = (time) => {
  let newTime, hour, minute, ampm, hourInt;

  hour = time.split(":")[0];
  minute = time.split(":")[1];
  hourInt = parseInt(hour);

  switch (true) {
    case hourInt === 0:
      {
        hour = "12";
        ampm = "A.M.";
      }
      break;
    case hourInt === 12:
      {
        ampm = "P.M.";
      }
      break;
    case hourInt > 12:
      {
        ampm = "P.M.";
        hourInt = hourInt - 12;
        if (hourInt < 10) {
          hour = `0${hourInt}`;
        } else {
          hour = `${hourInt}`;
        }
      }
      break;
    default:
      {
        ampm = "A.M.";
      }
      break;
  }

  newTime = `${hour}:${minute} ${ampm}`;
  return newTime;
};

export default TimeConverter;
