export const mainBgColor = "#f2f2f7";

export const primaryLight = "#6994ff";
export const primary = "#0d67f1";
export const primaryDark = "#003ebd";

export const secondaryLight = "#6aecfe";
export const secondary = "#23B9CB";
export const secondaryDark = "#00899a";

export const greywhite = "#e5e5ea";
export const greylight = "#d1d1d6";
export const grey = "#c7c7cc";
export const greydark = "#aeaeb2";
export const greyblack = "#8e8e93";
export const black = "black";

export const successColor = "#34C759";
export const errorColor = "#FF3B30";
