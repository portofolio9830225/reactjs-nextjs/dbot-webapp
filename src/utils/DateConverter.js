import isEmpty from "./isEmpty";

const DateConverter = (date) => {
  let newDate = "";
  if (!isEmpty(date)) {
    let day, month, monthName, year;

    day = date.split("-")[2];
    month = date.split("-")[1];
    year = date.split("-")[0];

    switch (month) {
      case "01":
      case "1":
        monthName = "Jan";

        break;
      case "02":
      case "2":
        monthName = "Feb";

        break;
      case "03":
      case "3":
        monthName = "Mar";

        break;
      case "04":
      case "4":
        monthName = "Apr";

        break;
      case "05":
      case "5":
        monthName = "May";

        break;
      case "06":
      case "6":
        monthName = "Jun";

        break;
      case "07":
      case "7":
        monthName = "Jul";

        break;
      case "08":
      case "8":
        monthName = "Aug";

        break;
      case "09":
      case "9":
        monthName = "Sep";

        break;
      case "10":
        monthName = "Oct";

        break;
      case "11":
        monthName = "Nov";

        break;
      case "12":
        monthName = "Dec";

        break;
    }

    newDate = `${day} ${monthName} ${year}`;
  }
  return newDate;
};

export default DateConverter;
