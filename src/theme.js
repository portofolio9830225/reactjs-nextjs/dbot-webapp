import { createMuiTheme } from "@material-ui/core/styles";
import { primary, primaryDark, primaryLight, secondary, secondaryDark, secondaryLight } from "./utils/ColorPicker";

export const theme = createMuiTheme({
	typography: {
		useNextVariants: true,
		fontFamily: "Inter, sans-serif",
	},
	palette: {
		primary: {
			light: primaryLight,
			main: primary,
			dark: primaryDark,
			contrastText: "#ffffff",
		},
		secondary: {
			light: secondaryLight,
			main: secondary,
			dark: secondaryDark,
			contrastText: "#fff",
		},
	},
	overrides: {
		MuiTooltip: {
			tooltip: {
				fontFamily: "Poppins",
				fontSize: "0.8rem",
				fontWeight: 400,
				letterSpacing: "0.8px",
			},
		},
		MuiDialog: {
			paperFullScreen: {
				height: "auto",
				maxHeight: "80vh",
				//width: "90vw",
				//borderRadius: "7px",
			},
		},
		MuiBackdrop: {
			root: {
				backgroundColor: "rgba(0,0,0,0.7)",
			},
		},
		MuiExpansionPanelSummary: {
			expandIcon: {
				color: "rgba(0,0,0,0)",
			},
		},
		MuiExpansionPanel: {
			root: {
				"&:before": {
					backgroundColor: "rgba(0,0,0,0)",
				},
			},
		},
		MuiCollapse: {
			container: {
				width: "100%",
			},
			wrapper: {
				width: "100%",
			},
			wrapperInner: {
				width: "100%",
			},
		},
		MuiTabs: {
			scroller: { padding: "0 0.1rem" },
		},
		MuiListItem: {
			root: {
				letterSpacing: "0.1rem",
			},
		},
		MuiFormLabel: {
			// root: {
			//   letterSpacing: "0.05rem",
			// },
		},
		MuiBottomNavigationAction: {
			root: {
				color: "rgba(0, 0, 0, 0.25)",
			},
		},
		MuiButton: {
			root: {
				fontWeight: 600,
				fontFamily: "Inter, sans-serif",
				letterSpacing: "0.15rem",
				borderRadius: "10px",
				"&$disabled": {
					borderColor: "rgba(0,0,0,0.30)",
				},
			},
			contained: {
				boxShadow: "none",
			},
			outlined: {},
			textPrimary: {
				"&:hover": {
					backgroundColor: "rgba(0,0,0,0)",
				},
			},
			// containedPrimary: {
			//   backgroundColor: "#000",
			//   border: "1px solid #000",
			//   color: "#fff",
			//   "&:hover": {
			//     border: "1px solid #000",
			//     backgroundColor: "rgba(0,0,0,0.7)",
			//   },
			// },
			// outlinedPrimary: {
			//   borderWidth: "1.5px",
			//   "&:hover": {
			//     borderWidth: "1.5px",
			//   },
			// },
			// containedSecondary: {
			//   backgroundColor: "#ab915d",
			//   border: "1px solid #ab915d",
			//   color: "#ab915d",
			//   "&:hover": {
			//     border: "1px solid #ab915d",
			//     backgroundColor: "rgba(171,145,93,0.75)",
			//   },
			// },
			// outlinedSecondary: {
			//   borderWidth: "1.5px",
			//   "&:hover": {
			//     borderWidth: "1.5px",
			//   },
			// },
		},
	},
});
