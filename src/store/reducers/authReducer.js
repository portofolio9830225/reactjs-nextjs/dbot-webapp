import isEmpty from "../../utils/isEmpty";

import { SET_CURRENT_USER, ACTIVATE_USER, LOGOUT, SET_CURRENT_BUSINESS, GET_STORE_DETAILS } from "../actions/index";

const initialState = {
	isAuthenticated: false,
	token: "",
	refreshToken: "",
	isBank: "",
	bankDetails: {},
	bankRejectDesc: "",
	message: "",
	business: {},
};

export default function (state = initialState, action) {
	switch (action.type) {
		case LOGOUT: {
			return {
				...state,
				isAuthenticated: false,
				token: "",
				refreshToken: "",
				isBank: "",
				bankDetails: {},
				bankRejectDesc: "",
				message: "",
			};
		}
		case SET_CURRENT_BUSINESS:
			return {
				...state,
				token: action.token,
				refreshToken: action.refreshToken,
			};
		case GET_STORE_DETAILS:
			return {
				...state,
				business: action.payload,
			};
		case SET_CURRENT_USER:
			return {
				...state,
				isAuthenticated: !isEmpty(action.payload),
				token: action.token,
				refreshToken: action.refreshToken,
			};

		case ACTIVATE_USER:
			return {
				...state,
				message: action.payload,
			};

		default:
			return state;
	}
}
