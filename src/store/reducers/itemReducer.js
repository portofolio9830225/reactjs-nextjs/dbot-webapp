import { GET_BULK_ITEM_LIST, GET_BULK_TRACKING_LIST } from "../actions";

const initialState = {
  bulkItemList: [],
  bulkTrackingData: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_BULK_ITEM_LIST:
      return {
        ...state,
        bulkItemList: action.payload,
      };
    case GET_BULK_TRACKING_LIST:
      return {
        ...state,
        bulkTrackingData: action.payload,
      };

    default:
      return state;
  }
}
