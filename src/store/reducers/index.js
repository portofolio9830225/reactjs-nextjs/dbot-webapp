import { combineReducers } from "redux";
import authReducer from "./authReducer";
import notiReducer from "./notiReducer";
import loadingReducer from "./loadingReducer";
import errorReducer from "./errorReducer";
import itemReducer from "./itemReducer";

export default combineReducers({
  auth: authReducer,
  item: itemReducer,
  loading: loadingReducer,
  error: errorReducer,
  noti: notiReducer,
});
