export const GET_ERROR = "GET_ERROR";
export const SET_INPUT_ERROR = "SET_INPUT_ERROR";
export const SET_LOADING = "SET_LOADING";
export const NOTIFICATION = "NOTIFICATION";
export const CLEAR_NOTIFICATION = "CLEAR_NOTIFICATION";

export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const SET_CURRENT_BUSINESS = "SET_CURRENT_BUSINESS";
export const GET_BUSINESS_LIST = "GET_BUSINESS_LIST";
export const GET_STORE_DETAILS = "GET_STORE_DETAILS";
export const ACTIVATE_USER = "ACTIVATE_USER";
export const LOGOUT = "LOGOUT";

export const GET_BULK_ITEM_LIST = "GET_BULK_ITEM_LIST";
export const GET_BULK_TRACKING_LIST = "GET_BULK_TRACKING_LIST";
