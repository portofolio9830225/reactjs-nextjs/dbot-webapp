import axios from "axios";
import { getError } from "./errorAction";

export const extractFile = (file) => async (dispatch) => {
  let data = {};
  await axios
    .get(file)
    .then((res) => {
      data = res.data;
    })
    .catch((err) => {
      dispatch(getError(err));
      console.log("cannot load file");
    });

  return data;
};
