import axios from "axios";
import isEmpty from "../../utils/isEmpty";

import {
	SET_CURRENT_USER,
	ACTIVATE_USER,
	GET_BUSINESS_LIST,
	GET_STORE_DETAILS,
	SET_CURRENT_BUSINESS,
	LOGOUT,
} from "./index";
import { setLoading } from "./loadingAction";
import { APILINK, XANO_API_LINK } from "../../utils/key";
import { customNotification, clearNotification } from "./notiAction";
import { getError, retryWithRefresh, setInputError } from "./errorAction";

export const sendUserEmail = data => dispatch => {
	dispatch(clearNotification());
	axios
		.post(`${XANO_API_LINK}/auth`, data)
		.then(res => {
			dispatch(customNotification(res.data.message, "success", "passcode"));
			//dispatch(setCurrentUser(res.data.token));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const loginUser = data => dispatch => {
	dispatch(clearNotification());
	axios
		.post(`${XANO_API_LINK}/auth/passcode`, data)
		.then(res => {
			dispatch(getBusinessList(res.data.user_id, res.data.user_token));
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const getBusinessList =
	(id = null, token = null) =>
	async dispatch => {
		let arr = [];
		if (isEmpty(token)) {
			dispatch({
				type: GET_BUSINESS_LIST,
				payload: [],
			});
		} else {
			dispatch(setLoading(true));
			await axios
				.get(`${XANO_API_LINK}/user/${id}/business`, {
					headers: { Authorization: token },
				})
				.then(res => {
					arr = res.data.business;
					if (arr.length === 0) {
						dispatch(customNotification("No business registered", "error"));
						dispatch(logoutUser());
					} else {
						if (arr.length === 1) {
							let data = {
								business_id: arr[0].id,
							};
							dispatch(getBusinessToken(data, token));
						} else {
							dispatch(setLoading(false));
						}

						dispatch({
							type: GET_BUSINESS_LIST,
							payload: arr,
						});
					}
				})
				.catch(err => {
					dispatch(setLoading(false));
					dispatch(getError(err));
				});
		}
		return arr;
	};

export const getBusinessToken = (data, token) => dispatch => {
	dispatch(setLoading(true));
	axios
		.post(`${XANO_API_LINK}/auth/business`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(
				setCurrentBusiness(data.business_id, res.data.user_id, res.data.access_token, res.data.refresh_token)
			);
		})
		.catch(err => {
			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const setCurrentBusiness =
	(bid = null, uid = null, data = null, refresh = null) =>
	dispatch => {
		dispatch(setBusinessToken(data, refresh));

		if (!isEmpty(data)) {
			dispatch(getStoreDetails(bid, data));
		} else {
			dispatch(getStoreDetails());
		}
	};

export const setBusinessToken =
	(data = null, refresh = null) =>
	dispatch => {
		// if (isEmpty(data)) {
		// 	AsyncStorage.removeItem("@dbot:businessToken");
		// } else {
		// 	AsyncStorage.setItem("@dbot:businessToken", data);
		// }
		// if (isEmpty(refresh)) {
		// 	AsyncStorage.removeItem("@dbot:refreshToken");
		// } else {
		// 	AsyncStorage.setItem("@dbot:refreshToken", refresh);
		// }

		dispatch({
			type: SET_CURRENT_BUSINESS,
			token: data,
			refreshToken: refresh,
		});
	};

export const getStoreDetails =
	(id = null, token = null) =>
	dispatch => {
		if (isEmpty(token)) {
			dispatch({
				type: GET_STORE_DETAILS,
				payload: {},
			});
		} else {
			axios
				.get(`${XANO_API_LINK}/business/${id}`, {
					headers: { Authorization: token },
				})
				.then(res => {
					dispatch({
						type: GET_STORE_DETAILS,
						payload: {},
					});

					dispatch({
						type: GET_STORE_DETAILS,
						payload: res.data,
					});
				})
				.catch(async err => {
					let retryData = await dispatch(retryWithRefresh(err));

					if (!isEmpty(retryData)) {
						dispatch(getStoreDetails(id, retryData.token));
					} else {
						dispatch(setLoading(false));
					}
				});
		}
	};

export const editStoreDetails = (id, data, token) => dispatch => {
	dispatch(setLoading(true));
	dispatch(clearNotification());

	axios
		.patch(`${XANO_API_LINK}/business/${id}`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(getStoreDetails(id, token));
			dispatch(customNotification(res.data.message, "success", "StoreSaved"));
			dispatch(setLoading(false));
		})
		.catch(async err => {
			console.log(err.response.data);
			let retryData = await dispatch(retryWithRefresh(err));

			if (!isEmpty(retryData)) {
				dispatch(editStoreDetails(id, data, retryData.token));
			} else {
				dispatch(setLoading(false));
			}
		});
};

export const setCurrentUser = data => dispatch => {
	dispatch({
		type: SET_CURRENT_USER,
		payload: data,
	});
};

export const logoutUser = () => dispatch => {
	dispatch({
		type: LOGOUT,
	});
	dispatch(setCurrentUser());
};

export const activateAccount = token => dispatch => {
	axios
		.post(`${XANO_API_LINK}/auth/activate`, null, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch({
				type: ACTIVATE_USER,
				payload: res.status,
			});
		})
		.catch(err => {
			dispatch({
				type: ACTIVATE_USER,
				payload: err.response.status,
			});
			dispatch(getError(err));
		});
};

export const registerBusiness = (data, token) => async dispatch => {
	dispatch(setLoading(true));
	dispatch(setInputError());

	axios
		.post(`${APILINK}/v1/business/account/registration`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));
			dispatch(customNotification(res.data.message, "success"));
		})
		.catch(err => {
			console.log("register err: ", err);
			if (err.response) {
				dispatch(setInputError(err.response.data.errors));
			}

			dispatch(setLoading(false));
			dispatch(getError(err));
		});
};

export const resetPassword = (pass, token) => dispatch => {
	dispatch(clearNotification());
	axios
		.post(`${XANO_API_LINK}/auth/reset-password`, pass, {
			headers: { Authorization: token },
		})
		.then(res => {
			dispatch(setLoading(false));
			dispatch(customNotification(res.data.message, "success"));
		})
		.catch(err => {
			dispatch(setLoading(false));
			if (err.response && err.response.status === 401) {
				dispatch(customNotification("Unauthorized", "error", "unauthorized"));
			} else {
				dispatch(getError(err));
			}
		});
};

export const forgotPassword = data => dispatch => {
	dispatch(clearNotification());
	axios
		.post(`${APILINK}/v1/user/account/forgot-password`, data)
		.then(res => {
			dispatch(setLoading(false));
			dispatch(customNotification(res.data.message, "success", "", false));
		})
		.catch(err => {
			dispatch(setLoading(false));
			if (err.response && err.response.status === 401) {
				dispatch(customNotification("Unauthorized", "error", "unauthorized"));
			} else {
				dispatch(getError(err));
			}
		});
};
