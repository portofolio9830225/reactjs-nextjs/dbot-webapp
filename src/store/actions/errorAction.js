import { GET_ERROR, SET_INPUT_ERROR } from "./index";
import { customNotification } from "./notiAction";
import store from "../index";
import { XANO_API_LINK } from "../../utils/key";
import axios from "axios";
import isEmpty from "../../utils/isEmpty";
import { setBusinessToken } from "./authAction";

export const getError = err => dispatch => {
	dispatch({
		type: GET_ERROR,
		payload: err,
	});
	if (!err.response) {
		dispatch(customNotification("Network not detected", "error"));
		console.log(err);
	} else {
		dispatch(customNotification(err.response.data.message, "error"));
		console.log(err.response.data);
	}
};

export const setInputError =
	(err = []) =>
	dispatch => {
		dispatch({
			type: SET_INPUT_ERROR,
			payload: err,
		});
	};

export const getNewAccessToken = (data, token) => async dispatch => {
	let val = {};

	await axios
		.post(`${XANO_API_LINK}/auth/refresh`, data, {
			headers: { Authorization: token },
		})
		.then(res => {
			console.log(res.data);
			val.refresh_token = res.data.refresh_token;
			val.token = res.data.access_token;
		})
		.catch(err => {
			if (err.response) {
				console.log(err.response.data);
			} else {
				console.log("No network detected (getNewAccessToken)");
			}
		});

	return val;
};

export const retryWithRefresh = err => async dispatch => {
	let data = {};
	let authStore = store.getState().auth;

	if (err.response && err.response.status === 401) {
		let tokenData = await dispatch(getNewAccessToken({ refresh_token: authStore.refreshToken }, authStore.token));
		if (!isEmpty(tokenData)) {
			data = tokenData;
			dispatch(setBusinessToken(tokenData.token, tokenData.refresh_token));
		}
	}

	return data;
};
