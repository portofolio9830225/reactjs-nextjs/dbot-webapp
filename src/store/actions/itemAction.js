import axios from "axios";
import isEmpty from "../../utils/isEmpty";

import { GET_BULK_ITEM_LIST, GET_BULK_TRACKING_LIST } from "./index";
import { setLoading } from "./loadingAction";
import { APILINK } from "../../utils/key";
import { customNotification, clearNotification } from "./notiAction";
import { getError } from "./errorAction";

export const extractItemFromCsv =
  (data = null, token) =>
  (dispatch) => {
    if (data === null) {
      dispatch({
        type: GET_BULK_ITEM_LIST,
        payload: [],
      });
    } else {
      axios
        .post(`${APILINK}/v1/item/bulk/upload`, data, {
          headers: { Authorization: token },
        })
        .then((res) => {
          setTimeout(() => {
            dispatch(setLoading(false));
            dispatch({
              type: GET_BULK_ITEM_LIST,
              payload: res.data.items,
            });
          }, 1000);
        })
        .catch((err) => {
          dispatch(setLoading(false));
          dispatch(getError(err));
        });
    }
  };

export const createItemBulkUpload = (data, token) => (dispatch) => {
  dispatch(setLoading(true));
  axios
    .post(`${APILINK}/v1/item/bulk/create`, data, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(customNotification(res.data.message, "success"));
    })
    .catch((err) => {
      dispatch(setLoading(false));
      dispatch(getError(err));
    });
};

export const extractTrackingFromCsv =
  (data = null, token) =>
  (dispatch) => {
    if (data === null) {
      dispatch({
        type: GET_BULK_TRACKING_LIST,
        payload: [],
      });
    } else {
      axios
        .post(`${APILINK}/v1/shipping/bulk/tracking/upload`, data, {
          headers: { Authorization: token },
        })
        .then((res) => {
          console.log(res.data);
          setTimeout(() => {
            dispatch(setLoading(false));
            dispatch({
              type: GET_BULK_TRACKING_LIST,
              payload: res.data,
            });
          }, 1000);
        })
        .catch((err) => {
          dispatch(setLoading(false));
          dispatch(getError(err));
        });
    }
  };

export const createTrackingBulkUpload = (data, token) => (dispatch) => {
  dispatch(setLoading(true));
  axios
    .patch(`${APILINK}/v1/shipping/bulk/tracking/update`, data, {
      headers: { Authorization: token },
    })
    .then((res) => {
      dispatch(setLoading(false));
      dispatch(customNotification(res.data.message, "success"));
    })
    .catch((err) => {
      dispatch(setLoading(false));
      dispatch(getError(err));
    });
};
